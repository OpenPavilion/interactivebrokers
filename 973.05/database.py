#!/usr/bin/env python
# -*- coding: utf-8 -*-
""" 
Tws Module
Created on 11 October 2017
@author: OpenPavilion

.. inheritance-diagram:: database
   :parts: 2

"""

import sqlite3

class IBDatabase:
    def __init__(self):
        self.initDatabase()
    
    def initDatabase(self):
        """ https://docs.python.org/3/library/sqlite3.html """
        self.DBconnection = sqlite3.connect('ibapi.db')
        self.DBcursor = self.DBconnection.cursor()
 
    def closeDatabase(self):
        self.DBconnection.commit()
        self.DBconnection.close()        