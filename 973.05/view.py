#!/usr/bin/env python
# -*- coding: utf-8 -*-
""" 
View Module

Created on 27 October 2017
@author: OpenPavilion

.. inheritance-diagram:: view
   :parts: 2
""" 

from tkinter import *
from tkinter import ttk
from _thread import *
from threading import Thread
import sys
import tkinter
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure
import os
import numpy as np
import pickle
from random import randint
from matplotlib.finance import candlestick_ohlc, candlestick2_ochl

from tws import *
from tkinter import messagebox

import matplotlib.dates as mdates
#from sympy.polys.polytools import quo
from plotly import *
import plotly.plotly as py 
#from Orange.widgets.gui import comboBox

import pickle


class View:
    def __init__(self, controller):
        self.controller = controller
        self.root = Tk()
        #self.root.iconbitmap("Balloon.ico")
        self.rootWindow = RootWindow(self, self.root)
    
    def runMainloop(self):
        self.root.title("OPTION BALLOONS -- TRADING SOFTWARE for Interactive Brokers")
        self.root.deiconify()
        from sys import platform
        if platform == "linux" or platform == "linux2":
            # linux
            self.root.iconbitmap("Balloon.ico")
        elif platform == "darwin":
            # OS X
            self.root.iconbitmap("Balloon.ico")
        elif platform == "win32":
            #self.root.iconbitmap("Balloon.ico")
            pass
        self.root.mainloop()


class RootWindow:
    """ 
    Das ist meine **Frame App** Klasse !!
    """
    def __init__(self, view, root):
        self.view = view
        self.frame = Frame(root)
        self.root = root  ## TODO: self.frame und self.root sind redundant
        self.productCollection = None  
        self.volaCharts = None
        self.stockChart = None
        self.setStyle('default')  ## ('clam','alt','default','classic')
        self.makeRootResziable()
        self.createMenu()
        self.createWidgets()
    
    def mainloop(self):
        self.mainloop()
    
    def setProductCollection(self, aProductCollection):
        """setProductCollection links to the ProductCollection from the Wrapper (TWSWrapper) class"""
        self.productCollection = aProductCollection
 
    def error(self, message):
        messagebox.showerror("Error", message)

    
    def setStyle(self, value):
        s=ttk.Style()
        s.theme_use(value)
    
    def createMenu(self):
        top = self.frame.winfo_toplevel()
        self.menuBar = tkinter.Menu(top)
        top['menu'] = self.menuBar
        self.subMenu = tkinter.Menu(self.menuBar)
        self.menuBar.add_cascade(label='Help', menu=self.subMenu)
        self.subMenu.add_command(label='About', command=self.redraw)
    
    def makeRootResziable(self):
        """"make root resizable"""
        top=self.frame.winfo_toplevel()
        top.rowconfigure(0, weight=1)
        top.columnconfigure(0, weight=1)
        self.frame.rowconfigure(0, weight=1)
        self.frame.columnconfigure(0, weight=1)
        self.frame.grid(sticky="NSEW")
    
    def createWidgets(self):
        """ Create Widgets """
        self.frame.grid(row=0, sticky=W)
        self.frame.bind("<Configure>", self.redraw)
        
        self.notebook=ttk.Notebook(self.frame) #, width= 400, height =400)
        self.notebook.bind_all("<<NotebookTabChanged>>", self.redraw)
        self.notebook.grid()
        
        self.page1 = ttk.Frame(self.frame)
        self.page2 = ttk.Frame(self.frame)
        self.page3 = ttk.Frame(self.frame)
        self.page4 = ttk.Frame(self.frame)
        
        self.notebook.add(self.page1, text='Account')
        self.notebook.add(self.page2, text='open Options')
        self.notebook.add(self.page3, text='Future Options')
        self.notebook.add(self.page4, text='Volatility')
        
        self.frame1 = Frame(self.page1, relief=RAISED, borderwidth=1)
        self.frame1.grid()
 
        self.frame2 = Frame(self.page2, relief=GROOVE, borderwidth=1)
        self.frame2.grid()
 
        self.frame3 = Frame(self.page3, relief=SUNKEN, borderwidth=1)
        self.frame3.grid()

        self.frame4 = Frame(self.page4, relief=SUNKEN, borderwidth=1)
        self.frame4.grid()
         
        self.netValueLabel = Label(self.frame1, text="Net Liquidation", bg="black", fg="white")
        self.netValueLabel.grid(row=0, column=0, sticky="WE")
        #self.netValueLabel.pack(side=LEFT)
        
        self.netLiquidationLabel = Label(self.frame1, 
                             text="Net Value", bg="white", fg="black")
        self.netLiquidationLabel.grid(row=1, column=0, sticky="WE")
        #self.netLiquidationButton.pack(side=LEFT)
 
        self.lookAheadMarginLabel = Label(self.frame1, text="Margin", bg="black", fg="white")
        self.lookAheadMarginLabel.grid(row=0, column=1, sticky="WE")
        #self.lookAheadMarginLabel.pack(side=LEFT)
        
        self.lookAheadMarginLabel = Label(self.frame1, 
                             text="Lookahead Margin", bg="white", fg="black")
        self.lookAheadMarginLabel.grid(row=1, column=1, sticky="WE")
        #self.lookAheadMarginButton.pack(side=LEFT)
        
        self.quoteColumnLabel = Label(self.frame1, text="Margin Quote", bg="black", fg="white")
        self.quoteColumnLabel.grid(row=0, column=2, sticky="WE")        
        
        self.quoteLabel = Label(self.frame1, 
                             text="QUIT", fg="black", bg="LightSkyBlue1")
        self.quoteLabel.grid(row=1, column=2, sticky="WE")
        #self.button.pack()#side=LEFT)
        
        #### self.hoverQuoteLabel = HoverInfo(self.quoteLabel, 'green < 25% margin = no fees \npink > 25% = margin fees \nred > 60% = critical margin reached')
 
 
        self.profitLabel = Label(self.frame1, text="Profit akt.", bg="midnight blue", fg="white")
        self.profitLabel.grid(row=0, column=3, sticky="WE")
        #self.profitLabel.pack(side=LEFT)
        
        self.profitButton = Label(self.frame1, 
                             text="Profit", fg="black")
        self.profitButton.grid(row=1, column=3, sticky="WE")
        #self.profitButton.pack(side=LEFT)   
         
        self.maxProfitLabel = Label(self.frame1, text="Profit max.", bg="midnight blue", fg="white")
        self.maxProfitLabel.grid(row=0, column=4, sticky="WE")
        #self.maxProfitLabel.pack(side=LEFT)
        
        self.maxProfitButton = Label(self.frame1, 
                             text="Profit", fg="black")
        self.maxProfitButton.grid(row=1, column=4, sticky="WE")
        #self.maxProfitButton.pack(side=LEFT)   
        
        self.riskLabel = Label(self.frame1, text="Risiko Depot", bg="midnight blue", fg="white")
        self.riskLabel.grid(row=0, column=5, sticky="WE")
        #self.maxProfitLabel.pack(side=LEFT)
                
        self.riskQuoteButton = Label(self.frame1, 
                             text="Risiko", fg="black")
        self.riskQuoteButton.grid(row=1, column=5, sticky="WE")
        #self.riskQuoteButton.pack(side=LEFT)                

        #### self.hoverQuoteButton = HoverInfo(self.riskQuoteButton, 'Risiko aller offenen Positionen im Depot') 
         
        self.pickleDumpButton = Button(self.frame1,
                             text="Save Data ...",
                             command=self.pickleDump)
        self.pickleDumpButton.grid(row=0, column=6, sticky=W)
        self.pickleLoadButton = Button(self.frame1,
                             text="Load Data ...",
                             command=self.pickleLoad)
        self.pickleLoadButton.grid(row=1, column=6, sticky=W)
        self.redrawButton = Button(self.frame1,
                             text="Redraw Charts ...",
                             command=self.redraw)
        self.redrawButton.grid(row=2, column=6, sticky=W)
        self.historicalDataRequestButton = Button(self.frame4,
                             text="Reload Vola ...",
                             command=self.trigger_volaChartData) 
        self.historicalDataRequestButton.grid(row=1, column=0, sticky=W)
        
        self.frame4.volaOptionVariable = StringVar(self.frame4)
        self.frame4.volaOptionMenu = OptionMenu(self.frame4, 
                                         self.frame4.volaOptionVariable, "1x Standardabweichung", "classic IVR")
        self.frame4.volaOptionVariable.set("1x Standardabweichung")
        self.frame4.volaOptionMenu.grid(row=1, column=1, sticky=W)
        #self.slogan.pack(side=LEFT)
        
        #self.underlyingPickerComboboxValue = StringVar()
        #self.underlyingPickerCombobox = ttk.Combobox(self.frame1, values=self.underlyingPickerComboboxValue, state='readonly', width=30, font=("Helvetica",10))
        self.underlyingPickerCombobox = ttk.Combobox(self.frame3, state='readonly', width=30, font=("Helvetica",10))
        self.underlyingPickerCombobox['values'] = ["initialisiere Handelklassen ..."]
        self.underlyingPickerCombobox.bind("<<ComboboxSelected>>", self.clickUnderlyingPickerCombobox)
        self.underlyingPickerCombobox.current(0)
        self.underlyingPickerCombobox.grid(row=0, column=7)
        #self.underlyingPickerCombobox.pack(side=LEFT)
         
        self.createTree()
        self.updatePies()
        self.createChart()
        self.createScatterChart()
        self.redrawStockChart()
        self.redrawVolatilityProduct()

    def trigger_volaChartData(self):
        #self.view.controller.tws.historicalDataRequests_req()
        self.view.controller.userAction.trigger_volaChartData()

    def pickleDump(self):
        self.view.controller.productCollection.pickleDump() 
        #pickle.dump(self.productCollection, open("save.data","wb"))
        
    def pickleLoad(self):
        self.view.controller.productCollection.pickleLoad() 
        #pickle.load(open("save.data","rb"))
        
        
    #===========================================================================
    # def redrawVolatilityProduct(self, aVolatilityProduct : OptionTrading_VolatilityProduct = None):
    #     self.volaFig = Figure(figsize=(3,2), dpi=100)
    #     self.plotVolatilityProduct(aVolatilityProduct, self.volaFig)
    #===========================================================================
    def redrawStockChart(self):
        if self.stockChart == None:
            self.stockChart = OptionAnalysisChart(self, self.frame3, 12,8,100)
            print("view.py - create new StockChart")
        self.stockChart.draw()

    def redrawVolatilityProduct(self, aVolatilityProduct : OptionTrading_VolatilityProduct = None): ## , volaFigure, volaAxis, volaCanvas, sizeX, sizeY, volaSubplot):
        if aVolatilityProduct != None:
            if self.volaCharts == None:
                self.volaCharts = VolatilityCharts(self.frame4, 11,8,100)
            if aVolatilityProduct.contractDetails.symbol == "VIX":
                self.volaCharts.addVolaChart(aVolatilityProduct.contractDetails.symbol, aVolatilityProduct, 331)
            if aVolatilityProduct.contractDetails.symbol == "OVX":
                self.volaCharts.addVolaChart(aVolatilityProduct.contractDetails.symbol, aVolatilityProduct, 332)
            if aVolatilityProduct.contractDetails.symbol == "TYVIX":
                self.volaCharts.addVolaChart(aVolatilityProduct.contractDetails.symbol, aVolatilityProduct, 333)
            if aVolatilityProduct.contractDetails.symbol == "EUVIX":
                self.volaCharts.addVolaChart(aVolatilityProduct.contractDetails.symbol, aVolatilityProduct, 334)
            if aVolatilityProduct.contractDetails.symbol == "GVZ":
                self.volaCharts.addVolaChart(aVolatilityProduct.contractDetails.symbol, aVolatilityProduct, 335)
            if aVolatilityProduct.contractDetails.symbol == "VXSLV":
                self.volaCharts.addVolaChart(aVolatilityProduct.contractDetails.symbol, aVolatilityProduct, 336)
            if aVolatilityProduct.contractDetails.symbol == "BPVIX":
                self.volaCharts.addVolaChart(aVolatilityProduct.contractDetails.symbol, aVolatilityProduct, 337)
            if aVolatilityProduct.contractDetails.symbol == "VXXLE":
                self.volaCharts.addVolaChart(aVolatilityProduct.contractDetails.symbol, aVolatilityProduct, 338)
            if aVolatilityProduct.contractDetails.symbol == "RVX":
                self.volaCharts.addVolaChart(aVolatilityProduct.contractDetails.symbol, aVolatilityProduct, 339)
                                
            self.volaCharts.draw()

    def updatePies(self, positions=None):
        import numpy as np
        import matplotlib.pyplot as plt
        from matplotlib.ticker import MultipleLocator, FormatStrFormatter
        from datetime import date
        from datetime import datetime
        import datetime

        if positions != None:
            if hasattr(self, 'pieFig'):
                self.pieFig.clf()             
                self.pieFig2.clf()             

            self.pieFig = Figure(figsize=(2,2), dpi=100)#plt.figure(figsize=(3,3), dpi=100)
            self.pieAx = self.pieFig.add_subplot(111)
            for item in ([self.pieAx.title, self.pieAx.xaxis.label, self.pieAx.yaxis.label] +
            self.pieAx.get_xticklabels() + self.pieAx.get_yticklabels()):
                item.set_fontsize(7)
    
            self.pieFig2 = Figure(figsize=(2,2), dpi=100)#plt.figure(figsize=(3,3), dpi=100)
            self.pieAx2 = self.pieFig2.add_subplot(111)
            for item in ([self.pieAx2.title, self.pieAx2.xaxis.label, self.pieAx2.yaxis.label] +
            self.pieAx2.get_xticklabels() + self.pieAx2.get_yticklabels()):
                item.set_fontsize(7)

            self.barFig3 = Figure(figsize=(4,2), dpi=100)#plt.figure(figsize=(3,3), dpi=100)
            self.barAx3 = self.barFig3.add_subplot(111, xlabel='Produkte', ylabel='Fortschritt')
            for item in ([self.barAx3.title, self.barAx3.xaxis.label, self.barAx3.yaxis.label] +
            self.barAx3.get_xticklabels() + self.barAx3.get_yticklabels()):
                item.set_fontsize(7)
            
            myArrProgress ={}                
            myArrDepot = {'Cash':float(self.getNetLiquidationLabel())}
            myArr = {}
            
            for ap in positions:
                if ap['contract'].symbol in myArr:
                    a = myArr[ap['contract'].symbol]
                    d = myArrDepot[ap['contract'].symbol]
                    p = myArrProgress[ap['contract'].symbol]
                else:
                    a = 0
                    d = 0
                    p = 0
                myArr.update({ap['contract'].symbol : a + float(ap['marketValue']*(-1))  })
                myArrDepot.update({ap['contract'].symbol : d + float(ap['averageCost'])*float(ap['position']) *(-1) })
                myArrProgress.update({ap['contract'].symbol : p + float(ap['unrealizedPNL'])  })

            self.pieAx.pie(myArrDepot.values(), labels=myArrDepot.keys(), shadow=True, autopct='%1.1f%%' , labeldistance=1.05) #, autopct='%1.1f%%')
            self.canvas = FigureCanvasTkAgg(self.pieFig, master=self.frame1)
            self.pieFig.suptitle("Klumpenrisiko im Depot", fontsize = 9)
            self.canvas.show() 
            self.canvas.get_tk_widget().grid(row=5, column=0) #, columnspan=3, rowspan=2, sticky=W+E+N+S)
            self.canvas.draw()

            self.pieAx2.pie(myArr.values(), labels=myArr.keys(), shadow=True, autopct='%1.1f%%' , labeldistance=1.05) #, autopct='%1.1f%%')
            self.canvas2 = FigureCanvasTkAgg(self.pieFig2, master=self.frame1)
            self.pieFig2.suptitle("Risiko pro Produkt", fontsize = 9)
            self.canvas2.show()
            self.canvas2.get_tk_widget().grid(row=5, column=1) #, columnspan=3, rowspan=2, sticky=W+E+N+S)
            self.canvas2.draw()

            self.barAx3.grid(True)
            self.barAx3.bar(myArrProgress.keys(), myArrProgress.values() )
            #self.barAx3.bar(myArrProgress.keys(), labels=myArrProgress.values())
            self.canvas3 = FigureCanvasTkAgg(self.barFig3, master=self.frame1)
            self.barFig3.suptitle("Gewinn/Verlust pro Produkt", fontsize = 9)
            self.canvas3.show()
            self.canvas3.get_tk_widget().grid(row=5, column=2) #, columnspan=3, rowspan=2, sticky=W+E+N+S)
            self.canvas3.draw() 

    def clickUnderlyingPickerCombobox(self, event=None):
        """clickUnderlyingPickerCombobox: der Anwender möchte die Optionskette zu einem Future erhalten (Anzahl der Handelsklassen steht in Klammer)
      
        .. uml::  
       
               @startuml
                  title Befüllen der Combobox
    
                    View -> UserAction: triggerTWStoFetchFutureHistoricalDataAndOptionChainGreeks
                    UserAction --> ProductCollection: triggerTWStoFetchFutureHistoricalDataAndOptionChainGreeks
                    ProductCollection --> Product: triggerTWStoFetchFutureHistoricalDataAndOptionChainGreeks
                    Product --> Future: triggerTWS_fetchGreeksForAllOptionChains
                    
                    Future --> TradingClass: setFutureOptionsInAllStrikes
                    TradingClass --> Strike: setFutureOptionsInAllStrikes
                    Strike --> FOP: setPutOption und setCallOption
                    
                    Future --> TradingClass: triggerTWS_fetchGreeksForAllOptionChains
                    TradingClass --> Strike: triggerTWS_fetchGreeksForAllOptionChains
                    Strike --> FOP: requestGreekDataFromTWS (call und put)
                    
               @enduml
          
        """
        self.view.controller.userAction.triggerTWStoFetchFutureHistoricalDataAndOptionChainGreeks( self.underlyingPickerCombobox.get(), self.stockChart )

         
    def updateUnderlyingPickerComboboxValue(self, oc):
        self.underlyingPickerCombobox['values'] = sorted(oc) ## self.underlyingPickerComboboxValue
        if len(oc) != 0:
            self.underlyingPickerCombobox.current(0)
        #print("underlyingPickerComboboxValue",self.underlyingPickerComboboxValue)



    def createScatterChart(self):  
        import numpy as np
        import matplotlib.pyplot as plt
        from matplotlib.ticker import MultipleLocator, FormatStrFormatter
        
        self.scatChart = Figure(figsize=(9, 5), dpi=100)
        self.scatChartPlot = self.scatChart.add_subplot(111)
       
        c = self.scatChartPlot.scatter([], [], s=[])
        c.set_alpha(0.5)
        
        #self.scatChartPlot.set_xlim(min(self.arrayX), max(self.arrayX))
        self.scatChartPlot.set_ylim(-120, 120)   
        self.scatChartPlot.axhline(0, color='blue', linewidth=1)
        self.scatChartPlot.axhline(80, color='green', linewidth=1, linestyle='--')
        self.scatChartPlot.axhline(-90, color='red', linewidth=1, linestyle='--')
        self.scatChartPlot.axvline(x=15, ymin=-120, ymax=120, color='orange', linewidth=1, linestyle='--')
        self.scatChartPlot.set_ylabel('in %')
        self.scatChartPlot.set_xlabel('USD')
        self.scatChartPlot.set_title('% Gewinn / Verlust der Positionen')
        self.scatChartPlot.grid(True)
        self.scatChartPlot.xaxis.set_major_locator(MultipleLocator(20))
        self.scatChartPlot.xaxis.set_major_formatter(FormatStrFormatter('%d'))
        self.scatChartPlot.xaxis.set_minor_locator(MultipleLocator(5))
        
        self.scatCanvas = FigureCanvasTkAgg(self.scatChart, master = self.frame2) # master=self.root)
        self.scatCanvas.show()
        self.scatCanvas.get_tk_widget().grid() #.pack(side='top', fill='both', expand=1)
#        canvas.get_tk_widget().pack(side=tkinter.TOP, fill=tkinter.BOTH, expand=1)

    def createChart(self):    
        import numpy as np
        import matplotlib.pyplot as plt
#         from matplotlib.ticker import MultipleLocator, FormatStrFormatter
#         
#         majorLocator = MultipleLocator(0.01)
#         majorFormatter = FormatStrFormatter('%f')
#         minorLocator = MultipleLocator(0.001)
                
        self.figChart = Figure(figsize=(9, 2), dpi=100)
        self.signal_axes = self.figChart.add_subplot(111)
        
        self.figChart.autofmt_xdate()            
        self.arrayX=[]
        self.arrayY=[]
        self.cost=[]

        self.lineChart, = self.signal_axes.plot(self.arrayX,self.arrayY, linewidth=2)
        self.signal_axes.set_ylabel('USD')
        self.signal_axes.set_xlabel('Zeit')        
        self.signal_axes.set_title('Verlauf der Gewinne / Verluste aller Positionen')
        self.signal_axes.grid(True)

#         self.signal_axes.xaxis.set_major_locator(majorLocator)
#         self.signal_axes.xaxis.set_major_formatter(majorFormatter)
#         self.signal_axes.xaxis.set_minor_locator(minorLocator)
        self.signal_axes.locator_params(nbins=5,axis='x')
        self.signal_axes.locator_params(nbins=5,axis='y')

        for item in ([self.signal_axes.title, self.signal_axes.xaxis.label, self.signal_axes.yaxis.label] + 
                     self.signal_axes.get_xticklabels() + self.signal_axes.get_yticklabels()):
            item.set_fontsize(8)
            
        self.chartCanvas = FigureCanvasTkAgg(self.figChart, master=self.frame2) # master=self.root)
        self.chartCanvas.show()
        self.chartCanvas.get_tk_widget().grid() #.pack(side='top', fill='both', expand=1)

    def appendChart(self,x,y, cost):
        self.arrayX.append(x)   #self.arrayX.append(x)
        self.arrayY.append(y)
        self.cost.append(cost)
        self.signal_axes.plot(self.arrayX,self.arrayY, linewidth=2)
        self.signal_axes.set_xlim(min(self.arrayX), max(self.arrayX))
        self.signal_axes.set_ylim(min(self.arrayY), max(self.arrayY)) 
        self.redraw()
    
    def redraw(self, event=None):    
        if hasattr(self, 'figChart'):
            self.figChart.canvas.draw()
        if hasattr(self, 'stockChart') and self.stockChart != None:
            self.stockChart.draw()  
        if hasattr(self, 'chartCanvas'):
            self.chartCanvas.draw()
        if hasattr(self, 'scatChart'):
           self.scatChart.canvas.draw()
        if hasattr(self, 'pieCanvas'):
            self.pieChart.canvas.draw()
        self.frame.update()


    def createTree(self):
        self.tree = ttk.Treeview(self.frame1, height = 15, columns=("lastTradeDateOrContractMonth", "position", "marketPrice", "marketValue", "averageCost", "unrealizedPNL", 'progress', 'realizedPNL', 'Risiko'), selectmode ="browse")
        #self.tree = ttk.Treeview(self.root, height = 5, columns=("lastTradeDateOrContractMonth", "position", "marketPrice", "marketValue", "averageCost", "unrealizedPNL", 'progress', 'realizedPNL'), selectmode ="browse")
        
        vsb = ttk.Scrollbar(self.frame1, orient="vertical", command=self.tree.yview)
        vsb.grid(row=3, column=7)
        
        self.tree.configure(yscrollcommand=vsb.set)
        
        self.tree.heading("#0", text='Contract', anchor='w')
        self.tree.heading("lastTradeDateOrContractMonth", text="last", anchor=tkinter.CENTER)
        self.tree.heading("position", text="position", anchor=tkinter.CENTER)
        self.tree.heading("marketPrice", text="marketPrice", anchor=tkinter.CENTER)
        self.tree.heading("marketValue", text="marketValue", anchor=tkinter.CENTER)
        self.tree.heading("averageCost", text="averageCost", anchor=tkinter.CENTER)
        self.tree.heading("unrealizedPNL", text="unrealizedPNL", anchor=tkinter.CENTER)
        self.tree.heading("progress", text="progress", anchor=tkinter.CENTER)
        self.tree.heading("realizedPNL", text="realizedPNL", anchor=tkinter.CENTER)
        self.tree.heading("Risiko", text="Risiko", anchor=tkinter.CENTER)
        
        self.tree.column("#0", anchor="w", minwidth=0,width=160, stretch=NO)
        self.tree.column("lastTradeDateOrContractMonth", anchor="e", minwidth=0,width=95, stretch=NO)
        self.tree.column("position", anchor="e", minwidth=0,width=50, stretch=NO)
        self.tree.column("marketPrice", anchor="e", minwidth=0,width=85, stretch=NO)
        self.tree.column("marketValue", anchor="e", minwidth=0,width=85, stretch=NO)
        self.tree.column("averageCost", anchor="e", minwidth=0,width=85, stretch=NO)
        self.tree.column("unrealizedPNL", anchor="e", minwidth=0,width=85, stretch=NO)
        self.tree.column("progress", anchor="e", minwidth=0,width=85, stretch=NO)        
        self.tree.column("realizedPNL", anchor="e", minwidth=0,width=85, stretch=NO)
        self.tree.column("Risiko", anchor="e", minwidth=0,width=85, stretch=NO)
        
        self.tree.insert("" , 0,    text="Loading Data from TWS ...", values=())
        self.tree.tag_configure('lawngreen', background='lawngreen')
        self.tree.tag_configure('orangered', background='orangered')
        self.tree.tag_configure('white', background='white')       
        #self.tree.pack(fill=BOTH, expand=1) 
        self.tree.grid(row=3, column=0, columnspan=7, rowspan=2, sticky=W+E+N+S)
        #self.tree.grid(row=2,columnspan=2) ##.pack()   


    def updateTreeItem(self, aPositions):
        import numpy as np
        import matplotlib.pyplot as plt
        from matplotlib.ticker import MultipleLocator, FormatStrFormatter
        from datetime import date
        from datetime import datetime
        import datetime

        self.updatePies(aPositions)
        
        self.scatChartPlot.cla()
        self.scatChartPlot.grid(True)
        self.scatChartPlot.set_ylim(-120, 120)   
        self.scatChartPlot.axhline(0, color='blue', linewidth=1)
        self.scatChartPlot.axhline(85, color='green', linewidth=2, linestyle='--')
        self.scatChartPlot.axhline(-90, color='red', linewidth=2, linestyle='--')
        self.scatChartPlot.axvline(x=7, ymin=-120, ymax=120, color='orange', linewidth=2, linestyle='-.')        
        self.scatChartPlot.set_ylabel('in %')
        self.scatChartPlot.set_xlabel('Restlaufzeit (Tage)')
        self.scatChartPlot.set_title('% Entwicklung der Positionen')
        self.scatChartPlot.xaxis.set_major_locator(MultipleLocator(7))
        self.scatChartPlot.xaxis.set_major_formatter(FormatStrFormatter('%d'))
        self.scatChartPlot.xaxis.set_minor_locator(MultipleLocator(1))    
        for x in self.tree.get_children():
            self.tree.delete(x)

        from sys import platform
        if platform == "linux" or platform == "linux2":
            # linux
            folderSign = "/"
        elif platform == "darwin":
            # OS X
            folderSign = "/"
        elif platform == "win32":
            folderSign = "\\"
        upIcon = os.getcwd() + folderSign +'pics'+ folderSign+'up32.png'
        downIcon = os.getcwd() + folderSign +'pics'+ folderSign+'down32.png'
        neutralIcon = os.getcwd() + folderSign +'pics'+ folderSign+'neutral32.png'
        self.upPic = PhotoImage(file=upIcon).subsample(2,2)
        self.downPic = PhotoImage(file=downIcon).subsample(2,2)
        self.neutralPic = PhotoImage(file=neutralIcon).subsample(2,2)
        xArr = []
        yArr = []
        sizeArr = []
        colArr=[]
        markerArr = []
        #for ap in aPositions:
        for ap in sorted(aPositions, key=lambda k: k['contract'].symbol) :
            if ( ap['position'] != 0 and ap['averageCost'] != 0):
                quotient = (float(ap['unrealizedPNL']) / (float(abs(ap['position']))* float(ap['averageCost'])) * 100)
                risiko = (float(abs(ap['position']))* float(ap['averageCost'])) * 100 / float(self.getNetLiquidationLabel())
                #print ("Quotient: " + quotient)
                if quotient >= 65: #85:   lawngreen   orangered   white
                    blueMarker = plt.imread('pics/balloon-green.png') 
                    markerArr.append(blueMarker)
                    self.tree.insert('', 'end', image=self.upPic, tags='white', text= str(" " + ap['contract'].symbol + ' / ' + ap['contract'].right + ' / ' + str(ap['contract'].strike)), values=(ap['lastTradeDateOrContractMonth'], '% 2i' % ap['position'], '% 2.5f' % ap['marketPrice'], ap['marketValue'], ap['averageCost'], ap['unrealizedPNL'], '% 2.2f %%' % quotient , ap['realizedPNL'], '% 2.2f %%' %  risiko))
                if quotient <= -65: #-90:
                    blueMarker = plt.imread('pics/balloon-red.png') 
                    markerArr.append(blueMarker)
                    self.tree.insert('', 'end', image=self.downPic, tags='white', text= str(" " + ap['contract'].symbol + ' / ' + ap['contract'].right + ' / ' + str(ap['contract'].strike)), values=(ap['lastTradeDateOrContractMonth'], '% 2i' % ap['position'], '% 2.5f' %  ap['marketPrice'], ap['marketValue'], ap['averageCost'], ap['unrealizedPNL'], '% 2.2f %%' % quotient , ap['realizedPNL'], '% 2.2f %%' %  risiko))
                if quotient > -65 and quotient < 65: # -90 and quotient < 85:
                    blueMarker = plt.imread('pics/balloon-neutral.png') 
                    markerArr.append(blueMarker)
                    self.tree.insert('', 'end', image=self.neutralPic, tags='white', text= str(" " + ap['contract'].symbol + ' / ' + ap['contract'].right + ' / ' + str(ap['contract'].strike)), values=(ap['lastTradeDateOrContractMonth'], '% 2i' % ap['position'], '% 2.5f' %  ap['marketPrice'], ap['marketValue'], ap['averageCost'], ap['unrealizedPNL'], '% 2.2f %%' % quotient , ap['realizedPNL'], '% 2.2f %%' %  risiko))
                datum = ap['lastTradeDateOrContractMonth']
                deltaDays = (date(int(datum[:4]), int(datum[4:6]), int(datum[6:8])) - datetime.date.today()).days
                xArr.append(deltaDays)   #(float(ap['averageCost']))
                yArr.append(float(quotient))
                
                #sizeArr.append( ap['averageCost'] * 25 / (int(deltaDays)+1) )
                sizeArr.append(abs(float(ap['unrealizedPNL'])))
                ####sizeArr.append( 1 )
                if quotient > 0:
                    rx = randint(0,40)
                    ry = randint(-25,-10)
                else:
                    rx = randint(-40,0)
                    ry = randint(-25,-10)
                    
                import math
                rx = math.sin(quotient/100*0.5*math.pi) * 50
                ry = math.cos(quotient/100*0.5*math.pi + (3.5*math.pi) ) * 50 - 50
                
                self.scatChartPlot.annotate(
                    str(ap['contract'].symbol + '/' + ap['contract'].right + '/' + str(ap['contract'].strike)), picker=True,
                    xy=(float(deltaDays), float(quotient)),
                    xytext=(ry, rx), fontsize=7,
                    textcoords='offset points', ha='right', va='bottom',
                    bbox=dict(boxstyle='round,pad=0.2', fc='yellow', alpha=0.7),
                    arrowprops=dict(arrowstyle = '->', connectionstyle='angle,angleA=0,angleB=90,rad=10')
                    )
                self.scatChartPlot.annotate(
                    '{:2.1f} %'.format(quotient), picker=True,
                    xy=(float(deltaDays), float(quotient)),
                    xytext=(ry,rx-10), fontsize=7,
                    textcoords='offset points', ha='right', va='bottom',
                    bbox=dict(boxstyle='round,pad=0.2', fc='white', alpha=0.7)
                    )
                if ap['unrealizedPNL'] > 0:
                    colArr.append('green')
                else:
                    colArr.append('red')
                  
#        self.scatChartPlot.fill_between(xArr,0,80, facecolor='limegreen', alpha=0.2)
        self.scatChartPlot.scatter(xArr, yArr, s=sizeArr, c=colArr, edgecolor='black', picker=True)
        self.scatChartPlot.plot(0, linestyle='-', color='blue', linewidth=2)

        #blueMarker = plt.imread('pics/hot_air_balloon.png')  #('pics/neutral32.png')
        self.plotImage(xArr, yArr, markerArr, self.scatChartPlot) 

        for item in ([self.scatChartPlot.title, self.scatChartPlot.xaxis.label, self.scatChartPlot.yaxis.label] + 
                     self.scatChartPlot.get_xticklabels() + self.scatChartPlot.get_yticklabels()):
            item.set_fontsize(8)
#        self.scatChartPlot.set_alpha(0.5)
        self.scatChart.canvas.draw()
        #self.scatCanvas.draw()
        #self.scatChartPlot.annotate(str(ap['contract'].symbol + '/' + ap['contract'].right + '/' + ap['contract'].strike), (ap['averageCost'],quotient))
        self.scatChart.canvas.mpl_connect('pick_event', self.onPick)

    ### Plots an image at each x and y location. 
    def plotImage(self, xData, yData, imData, ax):
        from matplotlib import pyplot as plt
        from matplotlib.image import BboxImage
        from matplotlib.transforms import Bbox, TransformedBbox        
        for x, y, im in zip(xData, yData, imData):
            bb = Bbox.from_bounds(x-3,y,6,30)  
            bb2 = TransformedBbox(bb,ax.transData)
            bbox_image = BboxImage(bb2,
                                norm = None,
                                origin=None,
                                clip_on=False)
            bbox_image.set_data(im)
            ax.add_artist(bbox_image)
    
    def onPick(self, event):
        from matplotlib.lines import Line2D
        from matplotlib.patches import Rectangle
        from matplotlib.text import Text
        from matplotlib.image import AxesImage
        
        print(event.ind, " clicked ", event)
        
        if isinstance(event.artist, Line2D):
            thisline = event.artist
            xdata = thisline.get_xdata()
            ydata = thisline.get_ydata()
            ind = event.ind
            print('onpick1 line:', zip(np.take(xdata, ind), np.take(ydata, ind)))
        elif isinstance(event.artist, Rectangle):
            patch = event.artist
            print('onpick1 patch:', patch.get_path())
        elif isinstance(event.artist, Text):
            text = event.artist
            print('onpick1 text:', text.get_text())

    def updateNetLiquidationLabel(self, value):
        self.netLiquidationLabel.configure(text=value)
        
    def getNetLiquidationLabel(self):
        return self.netLiquidationLabel.cget('text')
        #return self.netLiquidationButton.cget('text')
        
    def updateLookAheadMarginLabel(self, value):
        self.lookAheadMarginLabel.configure(text=value)
        
    def updateProfitButton(self, value):
        self.profitButton.configure(text=value)    
    
    def updateMaxProfitButton(self, value):
        self.maxProfitButton.configure(text=value)
    
    def updateRiskQuoteButton(self, value):
        self.riskQuoteButton.configure(text=value)
        if float(value[:-1]) < 2.5: ##remove %% from string and check for 60% margin
            if float(value[:-1]) < 1.25:
                self.riskQuoteButton.configure(bg='PaleGreen1')
            else:
                self.riskQuoteButton.configure(bg='RosyBrown1')
        else:
            self.riskQuoteButton.configure(bg="VioletRed1")
    
    def write_slogan(self):
        print ("Connecting ...")
        self.quoteLabel.configure(text="Connecting...", background="white")
#        self.connectToIB()

    def quit_frame(self):
        self.root.destroy()
        sys.exit()
          
    def updateQuote(self, value):
        self.quoteLabel.configure(text=value)
        if float(value[:-1]) < 60: ##remove %% from string and check for 60% margin
            if float(value[:-1]) < 25:
                self.quoteLabel.configure(bg='PaleGreen1')
            else:
                self.quoteLabel.configure(bg='RosyBrown1')
        else:
            self.quoteLabel.configure(bg="VioletRed1")
            
class HoverInfo(Menu):
    def __init__(self, parent, text, command=None):
       self._com = command
       Menu.__init__(self,parent, tearoff=0)
       if not isinstance(text, str):
          raise TypeError('Trying to initialise a Hover Menu with a non string type: ' + text.__class__.__name__)
       toktext=re.split('\n', text)
       for t in toktext:
          self.add_command(label = t)
          self._displayed=False
          self.master.bind("<Enter>",self.Display )
          self.master.bind("<Leave>",self.Remove )
        
     
    def __del__(self):
       self.master.unbind("<Enter>")
       self.master.unbind("<Leave>")
     
    def Display(self,event):
       if not self._displayed:
          self._displayed=True
          self.post(event.x_root, event.y_root)
       if self._com != None:
          self.master.unbind_all("<Return>")
          self.master.bind_all("<Return>", self.Click)
     
    def Remove(self, event):
     if self._displayed:
       self._displayed=False
       self.unpost()
     if self._com != None:
       self.unbind_all("<Return>")
     
    def Click(self, event):
       self._com()

#################################################################################################################
class OptionAnalysisChart:

    def __init__(self, aRootView, aFrame, sizeX, sizeY, theDpi):
        self.aRootView = aRootView
        self.containerFrame = aFrame
        self.sizeX = sizeX
        self.sizeY = sizeY
        self.theDpi = theDpi
        self.fig = Figure(figsize=(self.sizeX, self.sizeY), dpi=self.theDpi)   # neu anlegen

        self.canvas = FigureCanvasTkAgg(self.fig, master=self.containerFrame) # master=self.root)
        self.canvas.show()
        self.canvas.get_tk_widget().grid() #pack(side='top', fill='both', expand=1)
        
        #self.createStockChart()
        self.data = None

    def pickleDump(self):
        pickle.dump(self.data, open("optionAnalysis.data","wb"))
        with open("optionAnalysis.data", 'r', errors='replace') as f:
            print(f.readlines())
        
    def pickleLoad(self):
        self.data = pickle.load(open("optionAnalysis.data","rb"))
        with open("optionAnalysis.data", 'r', errors='replace') as f:
            print(f.readlines())
    
    def update(self, data, product):
        print("view.py - observer updated")
        self.data = data
        self.product = product
        self.pickleDump()
        self.draw()

    def moving_average(self, x, n, type='simple'):
        """
        compute an n period moving average.
    
        type is 'simple' | 'exponential'
    
        """
        x = np.asarray(x)
        if type == 'simple':
            weights = np.ones(n)
        else:
            weights = np.exp(np.linspace(-1., 0., n))
    
        weights /= weights.sum()
    
        a = np.convolve(x, weights, mode='full')[:len(x)]
        a[:n] = a[n]
        return a
    
#     def update(self, data):
#         print("view.py - observer updated")
#         self.data = data
#         self.pickleDump()
#         self.draw()
            
    def relative_strength(self, prices, n=14):
        """
        compute the n period relative strength indicator
        http://stockcharts.com/school/doku.php?id=chart_school:glossary_r#relativestrengthindex
        http://www.investopedia.com/terms/r/rsi.asp
        """
    
        deltas = np.diff(prices)
        seed = deltas[:n+1]
        up = seed[seed >= 0].sum()/n
        down = -seed[seed < 0].sum()/n
        rs = up/down
        rsi = np.zeros_like(prices)
        rsi[:n] = 100. - 100./(1. + rs)
    
        for i in range(n, len(prices)):
            delta = deltas[i - 1]  # cause the diff is 1 shorter
    
            if delta > 0:
                upval = delta
                downval = 0.
            else:
                upval = 0.
                downval = -delta
    
            up = (up*(n - 1) + upval)/n
            down = (down*(n - 1) + downval)/n
    
            rs = up/down
            rsi[i] = 100. - 100./(1. + rs)
    
        return rsi
    
    def get_cot(self, aKey):
        self.cot = pickle.load(open("quandloader.data","rb"))
        for a in self.cot:
            if aKey == a['key']:
                return a

    def draw(self):
        import numpy as np
        import matplotlib.pyplot as plt
        from matplotlib.ticker import MultipleLocator, FormatStrFormatter
        from datetime import date
        from datetime import datetime
        import datetime
        
        if self.data == None:
            print("no data to draw")
            return

        print("Produkt: ", self.product.get_cot_data()) 
        
        accountPositions = self.aRootView.view.controller.tws.accountUpdateService.getPositions(self.product.getSymbol())

        self.fig.clf()  

        ax = self.fig.add_subplot(2,1,2)
        ##ax = plt.subplot2grid((12, 1), (3, 0), rowspan=9)
        self.fig.suptitle(self.product.getSymbol())
        ax.grid(True)
                
        expiArr = []
        strikeArr= []
        strikeHighArr = []
        strikeLowArr = []
        sizeArr = []
        colArr = []
        #maArr = []
        donchMax = []
        donchMin = []
        donchVarMax = []
        donchVarMin = []
        tradeArr = []
        
        candleOpen = []
        candleClose = []
        candleHigh = []
        candleLow = []
        
        values = [{'strike':40, 'delta':0.01, 'premium':0.01},
                  {'strike':42, 'delta':0.02, 'premium':0.01},
                  {'strike':44, 'delta':0.04, 'premium':0.01},
                  {'strike':46, 'delta':0.08, 'premium':0.01},
                  {'strike':48, 'delta':0.13, 'premium':0.01},
                  {'strike':50, 'delta':0.18, 'premium':0.01},
                  {'strike':52, 'delta':0.24, 'premium':0.01},
                  {'strike':54, 'delta':0.37, 'premium':0.01},
                  {'strike':56, 'delta':0.51, 'premium':0.01},
                  {'strike':58, 'delta':0.61, 'premium':0.01},
                  {'strike':60, 'delta':0.69, 'premium':0.01},
                  {'strike':62, 'delta':0.75, 'premium':0.01},
                  {'strike':64, 'delta':0.82, 'premium':0.01},
                  {'strike':66, 'delta':0.87, 'premium':0.01},
                  {'strike':68, 'delta':0.91, 'premium':0.01},
                 ]
                         
        for a in self.data:
            candleOpen.append(a.open)
            candleClose.append(a.close)
            candleHigh.append(a.high)
            candleLow.append(a.low)
            expiArr.append(  datetime.date(int(a.date[:4]), int(a.date[4:6]), int(a.date[6:8])) )
            strikeArr.append(a.close)
            strikeHighArr.append(a.high)
            strikeLowArr.append(a.low)
            sizeArr.append((a.high - a.low)*10)
            if a.open < a.close:
                colArr.append("green")
            else:
                colArr.append("red")
            localMax = max(strikeHighArr[-90:])
            localMin = min(strikeLowArr[-90:])
            diff = localMax - localMin
            donchVarMax.append(a.close + diff)
            donchVarMin.append(a.close - diff)

            localMax = max(strikeHighArr[-90:])
            localMin = min(strikeLowArr[-90:])
            donchMax.append(localMax)
            donchMin.append(localMin)

            if a.high == localMax:
                tradeArr.append( {'x':-15, 'y':-20, 'text':'short Put' , 'date':datetime.date(int(a.date[:4]), int(a.date[4:6]), int(a.date[6:8])),'strike':localMin, 'color':"green"} )
            if a.low == localMin:
                tradeArr.append( {'x':-15, 'y':15, 'text':'short Call' , 'date':datetime.date(int(a.date[:4]), int(a.date[4:6]), int(a.date[6:8])),'strike':localMax, "color":"red"} )
                                
        #maArr = self.moving_average(strikeArr, 5, type='simple')
        
        ## plot die Kurve des Futures
        ax.plot(expiArr, strikeArr, linewidth=2, color="blue")
        #candlestick2_ochl(ax, candleOpen, candleClose, candleHigh, candleLow, width=2, colorup='k', colordown='r', alpha=0.75)

        ## zeichne den blauen Punkt des letzten Wertes
        x = [datetime.date.today(), datetime.date.today()]
        y = [strikeArr[-1], strikeArr[-1]]
        ax.stem(x,y, bottom=strikeArr[-1])
        
        ## zeichen die Kursdaten ein
        if expiArr[-1] > expiArr[-2]:
            annoCol = "green"
            dailySignum = "+"
        else:
            annoCol = "red"
            dailySignum = "-"
        ax.annotate(
            str(" " + str(strikeArr[-1]) + ' ( ' + dailySignum + '{:2.3f}'.format(strikeArr[-1]-strikeArr[-2]) + " )"), 
            xy=(datetime.date.today(), strikeArr[-1]),
            xytext=(10, -5), fontsize=9, color='white',
            textcoords='offset points', ha='left', va='bottom',
            bbox=dict(boxstyle='round,pad=0.2', fc=annoCol, alpha=0.7)
            #,arrowprops=dict(arrowstyle = '->', connectionstyle='angle,angleA=0,angleB=90,rad=10')
            )        
        
#        ax.plot(expiArr, donchVarMax, linewidth=1, linestyle=":", color="gray")
#        ax.plot(expiArr, donchVarMin, linewidth=1, linestyle=":", color="gray")

#        ax.fill_between(expiArr, donchVarMin, strikeArr, facecolor='gray', alpha=0.05, interpolate=True)
#        ax.fill_between(expiArr, donchVarMax, strikeArr, facecolor='gray', alpha=0.05, interpolate=True)        

        ax.plot(expiArr, donchMax, linewidth=2, linestyle=":", color="gray")
        ax.plot(expiArr, donchMin, linewidth=2, linestyle=":", color="gray")
                
        ax.fill_between(expiArr, donchMin, strikeArr, facecolor='green', alpha=0.1, interpolate=True)
        ax.fill_between(expiArr, donchMax, strikeArr, facecolor='red', alpha=0.1, interpolate=True)

        #ax.scatter(x=expiArr, y=strikeArr, c=colArr, s=sizeArr, edgecolor='black',  linewidth=1)
          

        for a in accountPositions:
            if a['unrealizedPNL'] > 0:
                col ="green"
                mySignum = "+"
            else:
                col="red"
                mySignum="-"
            dateA = a['lastTradeDateOrContractMonth']
            dateB = datetime.date( int(dateA[:4]), int(dateA[4:6]), int(dateA[6:8]) )
            ## oberer Text
            ax.annotate(
                str(mySignum + " " +str(a['unrealizedPNL']) ), 
                xy=(dateB, a['contract'].strike),
                xytext=(10, 0), fontsize=9, color='white',
                textcoords='offset points', ha='left', va='bottom',
                bbox=dict(boxstyle='round,pad=0.2', fc=col, alpha=0.8)
                #,arrowprops=dict(arrowstyle = '->', connectionstyle='angle,angleA=0,angleB=90,rad=10')
                )        
            ## unterer Text 
            ax.annotate(
                str(a['contract'].symbol + '/' + a['contract'].right + '/' + str(a['contract'].strike) + '/' + str(a['contract'].lastTradeDateOrContractMonth)), 
                xy=(dateB, a['contract'].strike),
                xytext=(10,-10), fontsize=7, 
                textcoords='offset points', ha='left', va='bottom',
                bbox=dict(boxstyle='round,pad=0.2', fc='white', alpha=0.7)
                )
            ## querlinie
            x = [datetime.date.today(), dateB]
            y = [a['contract'].strike, a['contract'].strike]
            ax.stem(x,y, bottom=a['contract'].strike, markerfmt='.', basefmt='k--')


        
        for a in values:
            ax.annotate(
                str('Δ {:2.3f}'.format(a['delta'])), 
                xy=(datetime.date(2017,11,29), a['strike']),
                xytext=(-10, 0), fontsize=6,
                textcoords='offset points', ha='right', va='bottom',
                bbox=dict(boxstyle='round,pad=0.2', fc="white", alpha=0.5),
                arrowprops=dict(arrowstyle = '->', connectionstyle='angle,angleA=0,angleB=90,rad=10')
                )

        for a in tradeArr:
            ax.annotate(
                a['text'], xy=(a['date'],  a['strike']),
                xytext=(a['x'],a['y']), fontsize=8, color='white',
                textcoords='offset points', ha='right', va='bottom',
                bbox=dict(boxstyle='round,pad=0.2', fc=a['color'], alpha=1),
                arrowprops=dict(arrowstyle = '->', connectionstyle='angle,angleA=0,angleB=90,rad=10')
                )
        
        #a = ax.axes([0.2, 0.6, .2, .2], facecolor='y')
        cot_gc = self.get_cot(self.product.getSymbol())
        cotDate = []
        cotLong = []
        cotShort = []
        cotShortDiff = []
        cotLongDiff = []
        cotNet = []
        cotIdxLong = []
        cotIdxShort = []
        
        longStudy = 156
        shortStudy = 26
        
        a = len(cot_gc['date'])
        i=0
        while i < a:
            if cot_gc['date'][i].date() > datetime.date(2016,6,1): #expiArr[0]: 
                if i == 0:
                    cotShortDiff.append(0)
                else:
                    cotShortDiff.append(cot_gc['short'][i] - cot_gc['short'][i-1])
                    cotLongDiff.append(cot_gc['long'][i] - cot_gc['long'][i-1])
                cotDate.append(cot_gc['date'][i])
                cotLong.append(cot_gc['long'][i] )
                cotShort.append(cot_gc['short'][i])
                cnet = cot_gc['long'][i] - cot_gc['short'][i]
                cotNet.append(cnet)
                
                cmaxShort = max(cotNet[-shortStudy:])
                cminShort = min(cotNet[-shortStudy:])
                cidxShort = 100 * ((cnet -cminShort)/(cmaxShort-cminShort))
                
                cmaxLong = max(cotNet[-longStudy:])
                cminLong = min(cotNet[-longStudy:])
                cidxLong = 100 * ((cnet -cminLong)/(cmaxLong-cminLong))

                cotIdxLong.append(cidxLong)
                cotIdxShort.append(cidxShort)
            i = i + 1

        ax2 = self.fig.add_subplot(12,1,1, sharex=ax)
        ##ax2 = plt.subplot2grid((12, 1), (0, 0), rowspan=1)
        ax2.grid(True)
        #ax2.axis([datetime.date(2016,11,1), datetime.date(2017,12,5), 0,100])
        ax2.plot(cotDate, cotIdxLong, cotDate, cotIdxShort)
        ax2.axhline(70, color='green', linewidth=1, linestyle='--')
        ax2.axhline(30, color='red', linewidth=1, linestyle='--')
        
        ax3 = self.fig.add_subplot(6,1,2, sharex=ax)
        ##ax3 = plt.subplot2grid((12, 1), (1, 0), rowspan=2)
        ax3.grid(True)
        ax3.plot(cotDate, cotShortDiff, color="red")
        ax3.plot(cotDate, cotLongDiff, color="blue")
        ax3.fill_between(cotDate, cotShortDiff, 0, where=cotShortDiff>cotLongDiff, facecolor='red', alpha=0.1, interpolate=True)        
        ax3.fill_between(cotDate, cotLongDiff, 0, where=cotLongDiff<cotShortDiff, facecolor='blue', alpha=0.1, interpolate=True)

        ax4 = self.fig.add_subplot(6,1,3, sharex=ax)
        ##ax3 = plt.subplot2grid((12, 1), (1, 0), rowspan=2)
        ax4.grid(True)
        rsi = self.relative_strength(strikeArr)
        fillcolor = 'darkgoldenrod'
        ax4.plot(expiArr, rsi, color=fillcolor)
        ax4.axhline(60, color='green', linewidth=1, linestyle='--')
        ax4.axhline(40, color='red', linewidth=1, linestyle='--')        
##        ax4.fill_between(expiArr, 40, rsi, where=rsi<40, facecolor='red', alpha=0.3, interpolate=True)        
##        ax4.fill_between(expiArr, 60, rsi, where=rsi>60, facecolor='green', alpha=0.3, interpolate=True)
        
        ##ax2.set_yscale('log')
#         ax.title('Impulse response')
#         ax.xlim(0, 0.2)
#         ax.xticks([])
#         ax.yticks([])

        
        #=======================================================================
        # self.canvas = FigureCanvasTkAgg(self.fig, master=self.containerFrame) # master=self.root)
        # self.canvas.show()
        # self.canvas.get_tk_widget().grid() #pack(side='top', fill='both', expand=1)
        #=======================================================================

        from datetime import  timedelta
        left = datetime.datetime.now() - timedelta(days=200)
        right = datetime.datetime.now() + timedelta(days=150)
        print ("date: ", left, right)
        ax.set_xlim([left, right ])
        ax.set_autoscale_on(False)
        ax.set_xbound(lower=left, upper=right)
        y_min, y_max = ax.get_ylim()
        ax.axvline(x=datetime.date.today(), ymin=y_min, ymax=y_max, color='orange', linewidth=1, linestyle='--')
        ax.axvline(x=right, ymin=y_min, ymax=y_max, color='orange', linewidth=1, linestyle=':')
                        
        ax2.set_xlim([left, right ])
        ax2.set_autoscale_on(False)
        ax2.set_xbound(lower=left, upper=right)
        y_min, y_max = ax2.get_ylim()
        ax2.axvline(x=datetime.date.today(), ymin=y_min, ymax=y_max, color='orange', linewidth=1, linestyle='--')
        
        ax3.set_xlim([left, right ])
        ax3.set_autoscale_on(False)
        ax3.set_xbound(lower=left, upper=right)
        y_min, y_max = ax3.get_ylim()
        ax3.axvline(x=datetime.date.today(), ymin=y_min, ymax=y_max, color='orange', linewidth=1, linestyle='--')
        
        ax4.set_xlim([left, right ])
        ax4.set_autoscale_on(False)
        ax4.set_xbound(lower=left, upper=right)
        y_min, y_max = ax4.get_ylim()
        ax4.axvline(x=datetime.date.today(), ymin=y_min, ymax=y_max, color='orange', linewidth=1, linestyle='--')

#===============================================================================
#         x = [1,2,3,4,5]
#         y= [1,1,1,1,1]
#         text = ["Price", "Vola", "Donchian", "COT", "RSI"]
#         #si = [300,100,100,100,100]
#         z = ["limegreen","limegreen","limegreen", "darkred", "limegreen"]
#  
#         ax5 = self.fig.add_subplot(10,5,50)
#         ax5.set_xlim(0,6)
#         ax5.xaxis.set_visible(False)
#         ax5.yaxis.set_visible(False)
#         ax5.scatter(x, y, s=600, c=z, marker=(5, 1), alpha=0.9, facecolor="black")
# 
#         i = 0
#         for a in x:
#             ax5.annotate(
#                 text[i], xy=(a, 1),
#                 xytext=(0, 20), fontsize=7, color='white',
#                 textcoords='offset points', ha='center', va='bottom',
#                 bbox=dict(boxstyle='round,pad=0.1')
#                 #,arrowprops=dict(arrowstyle = '->', connectionstyle='angle,angleA=0,angleB=90,rad=10')
#                 )
#             i = i + 1
#===============================================================================


              
        self.canvas.draw()

        
#===============================================================================
#     def drawOptionChains(self):
#         import numpy as np
#         import matplotlib.pyplot as plt
#         from matplotlib.ticker import MultipleLocator, FormatStrFormatter
#         from datetime import date
#         from datetime import datetime
#         import datetime
#         import matplotlib.dates as mdates
#         #print('refreshStockChart')
#         #oc = self.app.es_optionchain
# 
#         
#         expiArr = []
#         strikeArr= []
#         sizeArr = []
#         colArr = []
#         
#         self.stockChartPlot.cla()
#         self.stockChartPlot.grid(True)
#         for item in ([self.stockChartPlot.title, self.stockChartPlot.xaxis.label, self.stockChartPlot.yaxis.label] + 
#                      self.stockChartPlot.get_xticklabels() + self.stockChartPlot.get_yticklabels()):
#             item.set_fontsize(7)   
#               
#         for a in oc.options:
#             if 'expirationDate' in a and 'optPrice' in a and 'optionStrike' in a and 'delta' in a and 'undPrice' in a and 'optionRight' in a :
#                 if a['expirationDate'] is not None and a['optPrice'] is not None and a['optionStrike'] is not None and a['delta'] is not None and a['undPrice'] is not None and a['optionRight'] is not None :
#                     optionA = 'blue'
#                     if  a['undPrice'] < a['optionStrike'] and a['optionRight'] == "C":
#                         optionA = "green"
#                     if a['undPrice'] > a['optionStrike'] and a['optionRight'] == "P" :
#                         optionA = "red"     
#                     if optionA != "blue" and abs(a['delta']) < 1 and abs(a['delta'])  > 0:  
#                         futurePrice = a['undPrice']
#                         #print('a[delta]: ', abs(a['delta']))             
#                         dat = datetime.date(int(a['expirationDate'][:4]), int(a['expirationDate'][4:6]), int(a['expirationDate'][6:8]))
#                         doti = datetime.date.today()
#                         differ = dat - doti
#                         expiArr.append(dat)
#                         strikeArr.append(a['optionStrike'])
#                         ##print("Option Price, multiplier, days: ", a['optPrice'], int(oc.contract.multiplier), int(differ.days))
#                         sizeArr.append(a['optPrice'] * 10 * int(oc.contract.multiplier) / (int(differ.days)+1))
#                         colArr.append(optionA)  ## a['delta']
#                         if a['delta'] != None:
#                             self.stockChartPlot.annotate(
#                                 str('Δ {:2.3f}'.format(a['delta'])), 
#                                 xy=(dat, a['optionStrike']),
#                                 xytext=(-10, 0), fontsize=6,
#                                 textcoords='offset points', ha='right', va='bottom',
#                                 bbox=dict(boxstyle='round,pad=0.2', fc=optionA, alpha=0.5),
#                                 arrowprops=dict(arrowstyle = '->', connectionstyle='angle,angleA=0,angleB=90,rad=10')
#                                 )
#         if expiArr != []:
#             difference = (max(expiArr) - min(expiArr))*0.1
#             self.stockChartPlot.axhline(futurePrice, color='red', linewidth=1, linestyle='--')
#             self.stockChartPlot.axvline(x=datetime.date.today().strftime("%Y%m%d"), ymin=(min(strikeArr)*0.97), ymax=(max(strikeArr)*1.03), color='orange', linewidth=2, linestyle='--')
#             self.stockChartPlot.plot(expiArr, sizeArr, linewidth=1)
#             self.stockChartPlot.set_xlim(min(expiArr) - difference, max(expiArr) + difference)
#             self.stockChartPlot.set_ylim(min(strikeArr)*0.97, max(strikeArr)*1.03) 
#     
#     #         self.stockChartPlot.xaxis.set_major_locator(MultipleLocator(20))
#             self.stockChartPlot.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d'))
#     #         self.stockChartPlot.xaxis.set_minor_locator(MultipleLocator(5))
#                     
#             self.stockChartPlot.scatter(x=expiArr, y=strikeArr, s=sizeArr, c=colArr, edgecolor='black',  linewidth=1) ##c=colArr, 
#             self.stockChartPlot.plot(0, linestyle='-', color='blue', linewidth=1)
#             for item in ([self.stockChartPlot.title, self.stockChartPlot.xaxis.label, self.stockChartPlot.yaxis.label] + 
#                          self.stockChartPlot.get_xticklabels() + self.stockChartPlot.get_yticklabels()):
#                 item.set_fontsize(7)
#             self.stockChart.autofmt_xdate(rotation=90)
#===============================================================================

       
#################################################################################################################
class VolatilityCharts:
    
    def __init__(self, aFrame, sizeX, sizeY, theDpi):
        self.volaCharts = []
        self.containerFrame = aFrame
        self.sizeX = sizeX
        self.sizeY = sizeY
        self.theDpi = theDpi
        self.volaFig = Figure(figsize=(self.sizeX, self.sizeY), dpi=self.theDpi)   # neu anlegen
    
    def addVolaChart(self, symbol, aVolatilityProduct, subPlot):
        self.volaCharts.append({'symbol':symbol, 'aVolatilityProduct':aVolatilityProduct, 'subPlot':subPlot})
    
    def draw(self):
        import numpy as np
        import matplotlib.pyplot as plt
        from matplotlib.ticker import MultipleLocator, FormatStrFormatter
        from datetime import date
        from datetime import datetime
        import datetime
        from matplotlib.dates import DateFormatter, MonthLocator, YearLocator, WeekdayLocator, DayLocator, HourLocator, MONDAY
        from matplotlib.finance import candlestick_ochl
        from matplotlib.dates import date2num
        import matplotlib 
             
        if len(self.volaCharts) > 0:
            if self.volaFig != None:
##                plt.close(self.volaFig)
                self.volaFig.clf()             # löschen, weil schon gezeichnet und jetzt veraltet
            print(" ... draw beginn ....", datetime.datetime.now())
            for v in self.volaCharts:
                print("Schleife ", datetime.datetime.now(), v)
                v['ax'] = self.volaFig.add_subplot(v['subPlot'])
                for item in ([v['ax'].title, v['ax'].xaxis.label, v['ax'].yaxis.label] +
                v['ax'].get_xticklabels() + v['ax'].get_yticklabels()):
                    item.set_fontsize(7)
                print(" ... draw 1 ....", datetime.datetime.now())
                xArr=[]
                for a in v['aVolatilityProduct'].getBars():
                    if len(a.date) ==8:
                        dtt = datetime.date( int(a.date[0:4]), int(a.date[4:6]), int(a.date[6:8]) )
                        xArr.append([ dtt, a.open, a.close, a.high, a.low])
                        hfmt = matplotlib.dates.DateFormatter('%Y%m%d')
                    else:
                        xArr.append([date2num(datetime.datetime.strptime(a.date, '%Y%m%d %H:%M:%S')), a.open, a.close, a.high, a.low])
                        hfmt = matplotlib.dates.DateFormatter('%Y%m%d %H:%M:%S')
                    #===========================================================
                    # if len(a.date) ==8:
                    #     xArr.append([date2num(datetime.datetime.strptime(a.date, '%Y%m%d')), a.open, a.close, a.high, a.low])
                    #     hfmt = matplotlib.dates.DateFormatter('%Y%m%d')
                    # else:
                    #     xArr.append([date2num(datetime.datetime.strptime(a.date, '%Y%m%d %H:%M:%S')), a.open, a.close, a.high, a.low])
                    #     hfmt = matplotlib.dates.DateFormatter('%Y%m%d %H:%M:%S')
                    #===========================================================
                print(" ... draw 2 ....", datetime.datetime.now())
                #hfmt = matplotlib.dates.DateFormatter('%Y%m%d %H:%M:%S')
                v['ax'].xaxis.set_major_locator(YearLocator())
                v['ax'].xaxis.set_minor_locator(MonthLocator())
                v['ax'].xaxis.set_major_formatter(DateFormatter('%Y')) # %H:%M:%S'))
                v['ax'].xaxis.set_minor_formatter(DateFormatter('%m'))
                v['ax'].tick_params(axis = 'both', which = 'major', labelsize = 7)
                v['ax'].tick_params(axis = 'both', which = 'minor', labelsize = 6)
                v['ax'].xaxis_date()
                #v['ax'].autoscale_view()
                #plt.gcf().autofmt_xdate()
                v['ax'].grid(which = 'minor', alpha = 0.2)
                v['ax'].grid(which = 'major', alpha = 0.5)
                v['ax'].get_xaxis().set_tick_params(which='major', pad=6)
                v['ax'].get_xaxis().set_tick_params(which='minor', pad=-1)
                #plt.setp(v['ax'].get_xticklabels(), rotation=45)
                #self.volaFig.suptitle(v['aVolatilityProduct'].getContractDetails().symbol, fontsize = 10)
                self.volaFig.suptitle("IVR - 'implied volatility' :  Volatilität der Underlyings (12 Monate)", fontsize=12)
                print(" ... draw 3 ....", datetime.datetime.now())
                ### show IVR
                maxArr = max([el[2] for el in xArr])
                minArr = min([el[2] for el in xArr])
                print(" ... draw 4 ....", datetime.datetime.now())
                ivr = (xArr[-1][2]-minArr)/(maxArr-minArr)*100
                mittelWertFuenfTage = ((xArr[-2][2]+xArr[-3][2]+xArr[-4][2]+xArr[-5][2])/4)
                mittelPunkt =  ( (maxArr-minArr) / 2 ) + minArr
                steigung = ( xArr[-1][2] / mittelWertFuenfTage) * mittelPunkt
                offset =  mittelPunkt - xArr[-1][2]
                #print("Steigung: ", mittelPunkt, steigung, xArr[-1][2], mittelWertFuenfTage, maxArr, minArr)
                #v['ax'].annotate("5 Tages\nTendenz", xy=(len(xArr)+1, steigung), xytext=(int(len(xArr)/2), (maxArr-minArr)/2+minArr), arrowprops=dict(facecolor='blue', shrink=0.005))
                if xArr[-1][2] > mittelWertFuenfTage:
                    myFaceColor = "gold" ## "green"
                else:
                    if ivr > 40:
                        myFaceColor = "green"
                    else:
                        myFaceColor = "orangered"
                #### v['ax'].annotate("TEST", xy=(len(xArr)+1, xArr[-1][2]+offset), xytext=(len(xArr)-35, mittelWertFuenfTage+offset), arrowprops=dict(facecolor=myFaceColor, shrink=0.005))

                v['ax'].set_title(v['symbol']+ " / "+ v['aVolatilityProduct'].getParentProduct().getSymbol()+" (IVR:%2.1f %%)" %ivr)
                v['ax'].title.set_fontsize(7)
                print(" ... draw 5 ....", datetime.datetime.now())
                
                xA = [el[0] for el in xArr]
                yA = [el[2] for el in xArr]
                ## print(xA, yA)
                ##plt.plot(xA, yA, 'ro')
                v['ax'].plot(xA, yA, 'b-', lw=1)

                v['ax'].axhline(xArr[-1][2], color='black', linewidth=1, linestyle='--')
                bbox_props = dict(boxstyle="round4,pad=0.5", fc=myFaceColor, ec="black", lw=1, alpha=0.7)
                v['ax'].text(xArr[int(len(xArr)/2) ][0], xArr[-1][2], "%2.1f %%" % ivr, ha="center", va="center", rotation=0,size=7,bbox=bbox_props)

                if self.containerFrame.volaOptionVariable.get() != "classic IVR":
                    na = np.array(yA)
                    durchschnittsWert = np.mean(na)
                    standardAbweichung = np.std(na)
                    ivr2 = (xArr[-1][2] - (durchschnittsWert - standardAbweichung)) / (2 * standardAbweichung ) *100
                    v['ax'].axhline(durchschnittsWert, color='b', linewidth=1, linestyle='--')
                    v['ax'].axhline(durchschnittsWert - standardAbweichung, color='b', linewidth=1, linestyle='--')
                    v['ax'].axhline(durchschnittsWert + standardAbweichung, color='b', linewidth=1, linestyle='--')
                    v['ax'].axhline(xArr[-1][2], color='black', linewidth=1, linestyle='--')
                    if ivr2 > 40 and xArr[-1][2] > mittelWertFuenfTage:
                        myFaceColor = "green"
                    else:
                        myFaceColor = "red"
                    bbox_props = dict(boxstyle="round4,pad=0.5", fc=myFaceColor, ec="black", lw=1, alpha=0.9)
                    v['ax'].text(xArr[int(len(xArr)/2) ][0], xArr[-1][2], "%2.1f %%" % ivr2, color="white", ha="center", va="center", rotation=0,size=7,bbox=bbox_props)                    

                else:
                    v['ax'].axhline(xArr[-1][2], color='black', linewidth=1, linestyle='--')
                    bbox_props = dict(boxstyle="round4,pad=0.5", fc=myFaceColor, ec="black", lw=1, alpha=0.7)
                    v['ax'].text(xArr[int(len(xArr)/2) ][0], xArr[-1][2], "%2.1f %%" % ivr, ha="center", va="center", rotation=0,size=7,bbox=bbox_props)                    
                
                ##v['ax'].annotate("", xy=(xArr[-1][0], xArr[-1][2]+offset), xytext=(xArr[-45][0], mittelWertFuenfTage+offset), arrowprops=dict(facecolor=myFaceColor, shrink=0.005))

                #### weekday_quotes = [tuple([i]+list(quote[1:])) for i,quote in enumerate(xArr)]
                ####candlestick_ochl(v['ax'], weekday_quotes, width=0.01, colorup='g', colordown='r') 
                #candlestick_ochl(v['ax'], xArr, width=0.01, colorup='g', colordown='r') 
                
            #plt.tight_layout(pad=1.4, w_pad=1.5, h_pad=1.0)
                #v['ax'].tight_layout(pad=1.4, w_pad=1.5, h_pad=1.0)
                
            print(" ... draw 6 ....", datetime.datetime.now())
            volaCanvas = FigureCanvasTkAgg(self.volaFig, master=self.containerFrame)
            print(" ... draw 6a ....", datetime.datetime.now())
            volaCanvas.show()
            print(" ... draw 6b ....", datetime.datetime.now())
            volaCanvas.get_tk_widget().grid(row=2, column=0) #, columnspan=3, rowspan=2, sticky=W+E+N+S)
            print(" ... draw 6c ....", datetime.datetime.now())
            volaCanvas.draw()
            print(" ... draw 7 ....", datetime.datetime.now())
                
            print(" ... draw ende ....")