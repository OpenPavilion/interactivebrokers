#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
observer module

.. inheritance-diagram:: observer
   :parts: 2
"""
from abc import ABC, abstractmethod
 
#########################  Observer Pattern #########################
##        http://www.giantflyingsaucer.com/blog/?p=5117            ##
#########################  Observer Pattern #########################

class Observable(ABC):
    def __init__(self):
        self.observers = []
 
    def register(self, observer):
        if not observer in self.observers:
            self.observers.append(observer)
 
    def unregister(self, observer):
        if observer in self.observers:
            self.observers.remove(observer)
   
    def unregister_all(self):
        self.observers = []
 
    def update_observers(self, *args, **kwargs):
        for observer in self.observers:
            observer.update(*args, **kwargs)
        
class Observer(ABC):
    """must be implemented by Observer"""
    @abstractmethod
    def update(self, *args, **kwargs):
        pass        
    
#########################  Observer Pattern #########################
##        http://www.giantflyingsaucer.com/blog/?p=5117            ##
#########################  Observer Pattern #########################