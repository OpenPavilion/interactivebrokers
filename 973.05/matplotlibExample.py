""" 
View Module

Matplotlib Finance Example:
https://matplotlib.org/examples/pylab_examples/finance_work2.html

Bild im Bild:
https://matplotlib.org/examples/pylab_examples/axes_demo.html#pylab-examples-axes-demo

Unterachsen:
https://matplotlib.org/examples/pylab_examples/subplots_demo.html#pylab-examples-subplots-demo

Figure:
https://matplotlib.org/2.0.1/api/figure_api.html

Pyplot Tutorial:
https://matplotlib.org/users/pyplot_tutorial.html

Recipes:
https://matplotlib.org/users/recipes.html

""" 

from tkinter import *
from tkinter import ttk
from _thread import *
from threading import Thread
import sys
import tkinter
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure
import os
import numpy as np
import pickle
from random import randint

from tws import *
from tkinter import messagebox

import matplotlib.dates as mdates
import matplotlib.pyplot as plt
#from sympy.polys.polytools import quo
from plotly import *
import plotly.plotly as py 
#from Orange.widgets.gui import comboBox

import pickle
from matplotlib.widgets import Cursor


class View:
    def __init__(self):
        self.root = Tk()
        #self.root.iconbitmap("Balloon.ico")
        self.rootWindow = RootWindow(self, self.root)
    
    def runMainloop(self):
        self.root.title("OPTION BALLOONS -- TRADING SOFTWARE for Interactive Brokers")
        self.root.mainloop()

class RootWindow:
    """ 
    Das ist meine **Frame App** Klasse !!
    """
    def __init__(self, view, root):
        self.view = view
        self.root = root  ## TODO: self.frame und self.root sind redundant
        self.frame = Frame(self.root)
        self.productCollection = None  
        self.volaCharts = None
        self.stockChart = None
        #self.setStyle('default')  ## ('clam','alt','default','classic')
        self.createWidgets()
        self.redrawStockChart()
   
    def mainloop(self):
        self.mainloop()
        
    def createWidgets(self):
        """ Create Widgets """
        self.frame.grid(row=0, sticky=W)
        
        self.notebook=ttk.Notebook(self.frame) #, width= 400, height =400)
        self.notebook.grid()
        
        self.page1 = ttk.Frame(self.frame)
        self.page2 = ttk.Frame(self.frame)
        self.page3 = ttk.Frame(self.frame)
        self.page4 = ttk.Frame(self.frame)
        
        self.notebook.add(self.page1, text='Future Options')
        self.notebook.add(self.page2, text='Account')
        self.notebook.add(self.page3, text='open Options')
        self.notebook.add(self.page4, text='Volatility')
        
        self.frame1 = Frame(self.page3, relief=RAISED, borderwidth=1)
        self.frame1.grid()
 
        self.frame2 = Frame(self.page2, relief=GROOVE, borderwidth=1)
        self.frame2.grid()
 
        self.frame3 = Frame(self.page1, relief=SUNKEN, borderwidth=1)
        self.frame3.grid()

        self.frame4 = Frame(self.page4, relief=SUNKEN, borderwidth=1)
        self.frame4.grid()

        #self.frame.bind("<Configure>", self.redrawStockChart())
        #self.notebook.bind_all("<<NotebookTabChanged>>", self.redrawStockChart())

    def redrawStockChart(self):
        if self.stockChart == None:
            self.stockChart = OptionAnalysisChart(self.frame3, 15,9,100)
        self.stockChart.draw()


#################################################################################################################
class OptionAnalysisChart:

    def __init__(self, aFrame, sizeX, sizeY, theDpi):
        self.containerFrame = aFrame
        self.sizeX = sizeX
        self.sizeY = sizeY
        self.theDpi = theDpi
        self.fig = Figure(figsize=(self.sizeX, self.sizeY), dpi=self.theDpi)   # neu anlegen

        self.data = None
        self.pickleLoad()

#     def pickleDump(self):
#         pickle.dump(self.data, open("optionAnalysis.data","wb"))
#         with open("optionAnalysis.data", 'r', errors='replace') as f:
#             print(f.readlines())
        
    def pickleLoad(self):
        self.data = pickle.load(open("optionAnalysis.data","rb"))
        self.cot = pickle.load(open("quandloader.data","rb"))
#         with open("optionAnalysis.data", 'r', errors='replace') as f:
#             print(f.readlines())

    def moving_average(self, x, n, type='simple'):
        """
        compute an n period moving average.
    
        type is 'simple' | 'exponential'
    
        """
        x = np.asarray(x)
        if type == 'simple':
            weights = np.ones(n)
        else:
            weights = np.exp(np.linspace(-1., 0., n))
    
        weights /= weights.sum()
    
        a = np.convolve(x, weights, mode='full')[:len(x)]
        a[:n] = a[n]
        return a
    
#     def update(self, data):
#         print("view.py - observer updated")
#         self.data = data
#         self.pickleDump()
#         self.draw()

    def relative_strength(self, prices, n=14):
        """
        compute the n period relative strength indicator
        http://stockcharts.com/school/doku.php?id=chart_school:glossary_r#relativestrengthindex
        http://www.investopedia.com/terms/r/rsi.asp
        """
    
        deltas = np.diff(prices)
        seed = deltas[:n+1]
        up = seed[seed >= 0].sum()/n
        down = -seed[seed < 0].sum()/n
        rs = up/down
        rsi = np.zeros_like(prices)
        rsi[:n] = 100. - 100./(1. + rs)
    
        for i in range(n, len(prices)):
            delta = deltas[i - 1]  # cause the diff is 1 shorter
    
            if delta > 0:
                upval = delta
                downval = 0.
            else:
                upval = 0.
                downval = -delta
    
            up = (up*(n - 1) + upval)/n
            down = (down*(n - 1) + downval)/n
    
            rs = up/down
            rsi[i] = 100. - 100./(1. + rs)
    
        return rsi

    def get_cot(self, aKey):
        self.cot = pickle.load(open("quandloader.data","rb"))
        for a in self.cot:
            if aKey == a['key']:
                return a

    def draw(self):


        if self.data == None:
            return
        
        print("draw OptionAnalysisChart")

        self.fig.clf()  

        
        ax = self.fig.add_subplot(2,1,2)
        ##ax = plt.subplot2grid((12, 1), (3, 0), rowspan=9)
        ax.grid(True)
                
        expiArr = []
        strikeArr= []
        strikeHighArr = []
        strikeLowArr = []
        sizeArr = []
        colArr = []
        #maArr = []
        donchMax = []
        donchMin = []
        donchVarMax = []
        donchVarMin = []
        tradeArr = []
        
        values = [{'strike':40, 'delta':0.01, 'premium':0.01},
                  {'strike':42, 'delta':0.02, 'premium':0.01},
                  {'strike':44, 'delta':0.04, 'premium':0.01},
                  {'strike':46, 'delta':0.08, 'premium':0.01},
                  {'strike':48, 'delta':0.13, 'premium':0.01},
                  {'strike':50, 'delta':0.18, 'premium':0.01},
                  {'strike':52, 'delta':0.24, 'premium':0.01},
                  {'strike':54, 'delta':0.37, 'premium':0.01},
                  {'strike':56, 'delta':0.51, 'premium':0.01},
                  {'strike':58, 'delta':0.61, 'premium':0.01},
                  {'strike':60, 'delta':0.69, 'premium':0.01},
                  {'strike':62, 'delta':0.75, 'premium':0.01},
                  {'strike':64, 'delta':0.82, 'premium':0.01},
                  {'strike':66, 'delta':0.87, 'premium':0.01},
                  {'strike':68, 'delta':0.91, 'premium':0.01},
                 ]
                         
        for a in self.data:
            expiArr.append(  datetime.date(int(a.date[:4]), int(a.date[4:6]), int(a.date[6:8])) )
            strikeArr.append(a.close)
            strikeHighArr.append(a.high)
            strikeLowArr.append(a.low)
            sizeArr.append((a.high - a.low)*10)
            if a.open < a.close:
                colArr.append("green")
            else:
                colArr.append("red")
            localMax = max(strikeHighArr[-90:])
            localMin = min(strikeLowArr[-90:])
            diff = localMax - localMin
            donchVarMax.append(a.close + diff)
            donchVarMin.append(a.close - diff)

            localMax = max(strikeHighArr[-90:])
            localMin = min(strikeLowArr[-90:])
            donchMax.append(localMax)
            donchMin.append(localMin)

            if a.high == localMax:
                tradeArr.append( {'x':-15, 'y':-20, 'text':'short Put' , 'date':datetime.date(int(a.date[:4]), int(a.date[4:6]), int(a.date[6:8])),'strike':localMin, 'color':"green"} )
            if a.low == localMin:
                tradeArr.append( {'x':-15, 'y':15, 'text':'short Call' , 'date':datetime.date(int(a.date[:4]), int(a.date[4:6]), int(a.date[6:8])),'strike':localMax, "color":"red"} )
                                
        #maArr = self.moving_average(strikeArr, 5, type='simple')

        ax.plot(expiArr, strikeArr, linewidth=2, color="blue")
        
        ax.plot(expiArr, donchVarMax, linewidth=1, linestyle=":", color="gray")
        ax.plot(expiArr, donchVarMin, linewidth=1, linestyle=":", color="gray")

        ax.fill_between(expiArr, donchVarMin, strikeArr, facecolor='gray', alpha=0.03, interpolate=True)
        ax.fill_between(expiArr, donchVarMax, strikeArr, facecolor='gray', alpha=0.03, interpolate=True)        

        ax.plot(expiArr, donchMax, linewidth=2, linestyle=":", color="gray")
        ax.plot(expiArr, donchMin, linewidth=2, linestyle=":", color="gray")
                
        ax.fill_between(expiArr, donchMin, strikeArr, facecolor='green', alpha=0.03, interpolate=True)
        ax.fill_between(expiArr, donchMax, strikeArr, facecolor='red', alpha=0.03, interpolate=True)

        #ax.scatter(x=expiArr, y=strikeArr, c=colArr, s=sizeArr, edgecolor='black',  linewidth=1)
        
        for a in values:
            ax.annotate(
                str('Δ {:2.3f}'.format(a['delta'])), 
                xy=(datetime.date(2017,11,29), a['strike']),
                xytext=(-10, 0), fontsize=6,
                textcoords='offset points', ha='right', va='bottom',
                bbox=dict(boxstyle='round,pad=0.2', fc="white", alpha=0.5),
                arrowprops=dict(arrowstyle = '->', connectionstyle='angle,angleA=0,angleB=90,rad=10')
                )

        for a in tradeArr:
            ax.annotate(
                a['text'], xy=(a['date'],  a['strike']),
                xytext=(a['x'],a['y']), fontsize=8, color='white',
                textcoords='offset points', ha='right', va='bottom',
                bbox=dict(boxstyle='round,pad=0.2', fc=a['color'], alpha=1),
                arrowprops=dict(arrowstyle = '->', connectionstyle='angle,angleA=0,angleB=90,rad=10')
                )
        
        today = datetime.date.today()
        one_day = datetime.timedelta(days=150)
        dlate = today + one_day
        x = [datetime.date.today(), dlate]
        y = [a['strike'], a['strike']]
        ax.stem(x,y, bottom=a['strike'])
    
        #a = ax.axes([0.2, 0.6, .2, .2], facecolor='y')
        cot_gc = self.get_cot("GC")
        cotDate = []
        cotLong = []
        cotShort = []
        cotShortDiff = []
        cotLongDiff = []
        cotNet = []
        cotIdxLong = []
        cotIdxShort = []
        
        longStudy = 156
        shortStudy = 26
        
        a = len(cot_gc['date'])
        i=0
        while i < a:
            if cot_gc['date'][i].date() > datetime.date(2016,6,1): #expiArr[0]: 
                if i == 0:
                    cotShortDiff.append(0)
                else:
                    cotShortDiff.append(cot_gc['short'][i] - cot_gc['short'][i-1])
                    cotLongDiff.append(cot_gc['long'][i] - cot_gc['long'][i-1])
                cotDate.append(cot_gc['date'][i])
                cotLong.append(cot_gc['long'][i] )
                cotShort.append(cot_gc['short'][i])
                cnet = cot_gc['long'][i] - cot_gc['short'][i]
                cotNet.append(cnet)
                
                cmaxShort = max(cotNet[-shortStudy:])
                cminShort = min(cotNet[-shortStudy:])
                cidxShort = 100 * ((cnet -cminShort)/(cmaxShort-cminShort))
                
                cmaxLong = max(cotNet[-longStudy:])
                cminLong = min(cotNet[-longStudy:])
                cidxLong = 100 * ((cnet -cminLong)/(cmaxLong-cminLong))

                cotIdxLong.append(cidxLong)
                cotIdxShort.append(cidxShort)
            i = i + 1

        ax2 = self.fig.add_subplot(12,1,1, sharex=ax)
        ##ax2 = plt.subplot2grid((12, 1), (0, 0), rowspan=1)
        ax2.grid(True)
        #ax2.axis([datetime.date(2016,11,1), datetime.date(2017,12,5), 0,100])
        ax2.plot(cotDate, cotIdxLong, cotDate, cotIdxShort)
        ax2.axhline(70, color='green', linewidth=1, linestyle='--')
        ax2.axhline(30, color='red', linewidth=1, linestyle='--')
        
        ax3 = self.fig.add_subplot(6,1,2, sharex=ax)
        ##ax3 = plt.subplot2grid((12, 1), (1, 0), rowspan=2)
        ax3.grid(True)
        ax3.plot(cotDate, cotShortDiff, color="red")
        ax3.plot(cotDate, cotLongDiff, color="blue")
        ax3.fill_between(cotDate, cotShortDiff, 0, where=cotShortDiff>cotLongDiff, facecolor='red', alpha=0.1, interpolate=True)        
        ax3.fill_between(cotDate, cotLongDiff, 0, where=cotLongDiff<cotShortDiff, facecolor='blue', alpha=0.1, interpolate=True)

        ax4 = self.fig.add_subplot(6,1,3, sharex=ax)
        ##ax3 = plt.subplot2grid((12, 1), (1, 0), rowspan=2)
        ax4.grid(True)
        rsi = self.relative_strength(strikeArr)
        fillcolor = 'darkgoldenrod'
        ax4.plot(expiArr, rsi, color=fillcolor)
        ax4.axhline(60, color='green', linewidth=1, linestyle='--')
        ax4.axhline(40, color='red', linewidth=1, linestyle='--')        
        ax4.fill_between(expiArr, 40, rsi, where=rsi<40, facecolor='red', alpha=0.3, interpolate=True)        
        ax4.fill_between(expiArr, 60, rsi, where=rsi>60, facecolor='green', alpha=0.3, interpolate=True)
        
        ##ax2.set_yscale('log')
#         ax.title('Impulse response')
#         ax.xlim(0, 0.2)
#         ax.xticks([])
#         ax.yticks([])

        x = [1,2,3,4,5]
        y= [1,1,1,1,1]
        text = ["Price", "Vola", "Donchian", "COT", "RSI"]
        #si = [300,100,100,100,100]
        z = ["limegreen","limegreen","limegreen", "darkred", "limegreen"]
 
        ax5 = self.fig.add_subplot(10,5,50)
        ax5.set_xlim(0,6)
        ax5.xaxis.set_visible(False)
        ax5.yaxis.set_visible(False)
        ax5.scatter(x, y, s=600, c=z, marker=(5, 1), alpha=0.9, facecolor="black")

        i = 0
        for a in x:
            ax5.annotate(
                text[i], xy=(a, 1),
                xytext=(0, 20), fontsize=7, color='white',
                textcoords='offset points', ha='center', va='bottom',
                bbox=dict(boxstyle='round,pad=0.1')
                #,arrowprops=dict(arrowstyle = '->', connectionstyle='angle,angleA=0,angleB=90,rad=10')
                )
            i = i + 1
        
        self.canvas = FigureCanvasTkAgg(self.fig, master=self.containerFrame) # master=self.root)
        self.canvas.show()
        self.canvas.get_tk_widget().grid() #pack(side='top', fill='both', expand=1)




if __name__ == '__main__':
    v = View()
    v.runMainloop()
    
    
    
    
    
#             self.stockChartPlot.cla()
#             self.stockChartPlot.grid(True)
# 
#             self.stockChartPlot.plot_date(expiArr, sizeArr, linewidth=1)
#                 
#             self.stockChartPlot.axhline(100, color='red', linewidth=1, linestyle='--')
#             self.stockChartPlot.axvline(x=datetime.date.today().strftime("%Y%m%d"), ymin=(min(strikeArr)*0.97), ymax=(max(strikeArr)*1.03), color='orange', linewidth=2, linestyle='--')
#             #c = self.stockChartPlot.scatter(x=expiArr, y=strikeArr, edgecolor='black',  linewidth=1) ## s=sizeArr, c=colArr, 
#             #c.set_alpha(0.5)
#             #self.stockChartPlot.set_xlim(min(expiArr), max(expiArr))
#             self.stockChartPlot.set_ylim(min(strikeArr)*0.97, max(strikeArr)*1.03) 
#     
#             #self.stockChartPlot.xaxis.set_major_locator(MultipleLocator(20))
#             self.stockChartPlot.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d'))
#             #self.stockChartPlot.xaxis.set_minor_locator(MultipleLocator(5))
# 
# 
# 
#                     
#             #self.stockChartPlot.plot(0, linestyle='-', color='blue', linewidth=1)
#             #for item in ([self.stockChartPlot.title, self.stockChartPlot.xaxis.label, self.stockChartPlot.yaxis.label] + 
#             #              self.stockChartPlot.get_xticklabels() + self.stockChartPlot.get_yticklabels()):
#             #     item.set_fontsize(7)
# 
#             self.stockCanvas = FigureCanvasTkAgg(self.stockChart, master=self.containerFrame) # master=self.root)
#             self.stockCanvas.show()
#             self.stockCanvas.get_tk_widget().grid() #pack(side='top', fill='both', expand=1)
#             #self.stockChart.autofmt_xdate(rotation=90)
        
        # plot