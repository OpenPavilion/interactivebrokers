#!/usr/bin/env python
# -*- coding: utf-8 -*-
""" 
The MAIN module starts the application and runs in an infinite loop.

*Preconditions*
- Python 3.6 or higher
- Anaconda 3, activcate orange3
- pip install: matplotlib, plotly, quandl
- install IBAPI (Interactive brokers)
 

.. uml::  
    
   @startuml
   title Module Overview
    main --> Controller : main instantiates the Controller
    note left
        The Controller creates the major elements
        * TWS Wrapper
        * Model
        * View
    end note
    Controller: self.model = Model(self)
    Controller: self.view = View(self); self.view.run()
    Controller --> Model : self.model.changeData()
    Model --> Controller: self.controller.updateView()
    Controller --> View : self.view.updateViewElement()
    View --> Controller : self.controller.elementTrigger()
    Model --> TWSWrapper : self.wrapper.fetchData()
    TWSWrapper --> Model : self.model.dataFetched()
    Model: self.controller
    Model: self.wrapper = TWSWrapper(self)
    View: self.controller
    TWSWrapper: self.model
    @enduml
    

.. uml::  
   
   @startuml

   title Classes Overview
 
   header
    <font color=red>Warning:</font>
    Do not use in production.
   endheader
 
   package Controller <<Folder>> {
   class Controller {
   .. internal fields ..
   -view
   -tws
   -productCollection
   .. public methods ..
   +void connectToIB()
   +void runMainloop()
   }
   }
 
   package TWS <<Folder>> {
   class TWSWrapper {
   +error() -> gibt Fehler zurueck
   +connectAck() -> verbindet mit TWS
   }
   class TWSService {
   +addOserver()
   +removeObserver()
   +setChanged() 
   +update()
   }
   class TWSContractDetailService {
   +contractDetails()
   }
   TWSService <|-- TWSContractDetailService
 
   class TWSAccountUpdateService {
   +accountUpdate()
   +managedAccounts()
   }
   TWSService <|-- TWSAccountUpdateService
 
   class TWSPortfolioUpdateService {
   +updatePortfolio()
   }
   TWSService <|-- TWSPortfolioUpdateService
 
   class TWSOptionChainService {
   +securityDefinitionOptionParamater()
   +securityDefinitionOptionParamaterEnd()
   }
   TWSService <|-- TWSOptionChainService
 
   class TWSHistoricalDataService {
   +historicalData()
   +historicalDataEnd()
   }
   TWSService <|-- TWSHistoricalDataService
 
   class TWSConnectService {
   +connectAck()
   +reconnect()
   +close()
   }
   TWSService <|-- TWSConnectService
 
   }
   TWSWrapper ..> TWSContractDetailService: calls
   package Model <<Folder>> {
   class OptionTrading_Product_Collection {
   -__init__()
   }
   abstract class OptionTrading {
   -subscribeToService()
   }
   class OptionTrading_Product {
   -__init__()
   }
   class OptionTrading_VolatilityProduct {
   -__init__() 
   }
   class OptionTrading_Future {
   -__init__()
   }
   }  
   OptionTrading <|-- OptionTrading_Product
   OptionTrading <|-- OptionTrading_VolatilityProduct
   OptionTrading <|-- OptionTrading_Future
 
   package View <<Folder>> {
   class View {
   }
   class RootView {
   }
   }  
   
    @enduml
"""
################################################################################################################
##  command line:  pycallgraph graphviz -- ./MAIN.py
## from pycallgraph import PyCallGraph
## from pycallgraph.output import GraphvizOutput
################################################################################################################

from controller import *  ## needed to start the controller
import ibapi  ## needed to show API version that we are using

############################################################################################################### 
 
def main():
    """
    The main function starts the application initializing the controller.
    """
    print("---------------------------------------------------------------------------")
    print("  OPTION BALLOONS -- TRADING SOFTWARE")
    print("---------------------------------------------------------------------------")
    print("  TWS API Version: ", ibapi.__version__, " Folder:" , os.path.relpath(".",".."))
    print("---------------------------------------------------------------------------")
    c=Controller()
    c.main_step_001_connectToIB()
    c.main_step_002_runMainloop()
    
if __name__ == '__main__':
    main()
  
###############################################################################################################