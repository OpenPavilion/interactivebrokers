.. Docu documentation master file, created by
   sphinx-quickstart on Fri Oct 13 09:04:50 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Dokumentation für OpenPavilion
================================

.. toctree::
   :maxdepth: 4
   :caption: Contents:

   controller.rst
   model.rst
   main.rst
   view.rst
   tws.rst
   observer.rst

 
Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`



Installationshinweis
=========================
Die Applikation kann gerufen werden von der Console.
Voraussetzungen: Python 3.6, anaconda 3, IBAPI (Interactive brokers)