#!/usr/bin/env python
# -*- coding: utf-8 -*-
""" 
Tws Module

Created on 11 October 2017
@author: OpenPavilion

.. inheritance-diagram:: tws
   :parts: 2

"""

from ibapi import *
from ibapi.wrapper import EWrapper
from ibapi.client import EClient
from ibapi.utils import iswrapper
from ibapi.common import *
from ibapi.contract import *
from ibapi.order import *
from ibapi.order_state import *
from ibapi.ticktype import *
from datetime import datetime
import sqlite3

from model import *
from database import IBDatabase

#import pymysql
from datetime import datetime
import datetime
from time import gmtime, strftime
import time
from time import sleep

from observer import *

MINIMUM_INT = 10000

        
class TWSWrapper(EWrapper, EClient):
  """
  TWSWrapper ist der Wrapper für den Zugriff auf die TWS.
  """  
  def __init__(self, aController, rootWindow):
    """__init__ initialisiert den Wrapper"""
    self.controller = aController
    self.rootWindow = rootWindow
    
    self.accountUpdateService = None   ## Observable, empty by default; will be set by controller as soon as connection is established and Account is known
    
    self.requestId = MINIMUM_INT
    self.accountPositions = list()
    EClient.__init__(self,wrapper=self)


  def error(self, reqId:TickerId, errorCode:int, errorString:str):
    if errorCode == 502:
        self.rootWindow.error(str(errorCode) + ": " +errorString)
    print('Error: ',  reqId, " ", errorCode, " ", errorString)
    
  def getNextRequestId(self):
    self.requestId = self.requestId + 1
    return self.requestId

  def connectAck(self):
    print("\n[connected to TWS]") 
    execString = "INSERT INTO LOG VALUES (NULL, 'Connected to TWS at: " + str(datetime.datetime.now()) + "')"
    print(execString)
    db = IBDatabase()
    db.DBcursor.execute(execString)
    db.closeDatabase()
    
    self.controller.tws_step_003_connectAck()
    #self.es_optionchain = OptionChain("ES", "FUT", "GLOBEX", "USD", "50")
    #self.reqContractDetails(209, self.es_optionchain.getBaseContract())  ## ==> contractDetails
 

  def tickOptionComputation(self, reqId: TickerId, tickType: TickType, impliedVol: float, delta: float, optPrice: float, pvDividend: float, gamma: float, vega: float, theta: float, undPrice: float):
     if reqId > MINIMUM_INT and tickType == 13 and delta != None and optPrice != None:
          self.es_optionchain.setOptionTickData(reqId, impliedVol, delta, optPrice, gamma, vega, theta, undPrice)
     #==========================================================================
     # if reqId == 1013:
     #    print("TickOptionComputation. TickerId:", reqId, "tickType:", tickType,
     #      "ImpliedVolatility:", impliedVol, "Delta:", delta, "OptionPrice:",
     #     optPrice, "pvDividend:", pvDividend, "Gamma: ", gamma, "Vega:", vega,
     #     "Theta:", theta, "UnderlyingPrice:", undPrice)
     #==========================================================================

          #print('..... Greeks received: ', reqId, tickType, impliedVol, delta, optPrice, gamma, vega, theta, undPrice)
#      if tickType == 13 and optPrice is not None and undPrice is not None and impliedVol is not None and delta is not None:
#          print("tickOptionComputation: Type: ", tickType, 
#                " ReqId: ",    reqId," ",  "OptionPrice: % 3.2f " % optPrice, 
#                "UnderlyingPrice: % 4.2f " % undPrice, " IV: % 2.1f" % impliedVol, 
#                " Delta: % 1.3f " % delta)
#      if (tickType == 10 or tickType == 11) and optPrice is not None and undPrice is not None and impliedVol is not None and delta is not None:
#          print("TickOptionComputation. TickerId:", reqId, "tickType:", tickType,
#                "ImpliedVolatility: % 2.1f" % (impliedVol * 100), "Delta: % 0.3f" % delta, 
#                "OptionPrice: % 4.5f" % optPrice, "UnderlyingPrice: % 6.2f" % undPrice)
#      if tickType == 12 and optPrice is not None and gamma is not None and theta is not None and delta is not None and undPrice is not None:
#          print("TickOptionComputation. TickerId:", reqId, "tickType:", tickType,
#                "Delta: % 2.3f" % delta, 
#                "Gamma: % 2.3f" % gamma, 
#                "Theta: % 2.3f" % theta, "UnderlyingPrice: % 6.2f" % undPrice)


  def tickSnapshotEnd(self, reqId: int):
     self.rootWindow.refreshStockChart(self.es_optionchain)
     #print("TickSnapshotEnd - refreshStockChart:", reqId)



  def updateAccountValue(self, key: str, val: str, currency: str,
                       accountName: str):
    self.accountUpdateService.updateAccountValue(key, val, currency, accountName)

  def updatePortfolio(self, contract: Contract, position: float,
                      marketPrice: float, marketValue: float,
                      averageCost: float, unrealizedPNL: float,
                      realizedPNL: float, accountName: str):
    self.accountUpdateService.updatePortfolio(contract, position,
                      marketPrice, marketValue,
                      averageCost, unrealizedPNL,
                      realizedPNL, accountName)

  def position(self, account: str, contract: Contract, position: float,
               avgCost: float):
    self.accountUpdateService.position(account, contract, position, avgCost)
    
  def positionEnd(self):
    self.accountUpdateService.positionEnd()

  def accountDownloadEnd(self, accountName: str):
      self.accountUpdateService.accountDownloadEnd(accountName)
   #### print("tws.py - Account download finished:", accountName)
   #### self.controller.accountDownloadEnd(accountName)

    ###### self.reqAllOpenOrders()  
    ####self.reqMktData(1013, FOPContracts.VIX_IDX(), "", True, False, [] ) ## ==> tickOptionComputation
    #self.realTimeBars_req()
    
  #=============================================================================
  # def realTimeBars_req(self):
  #   self.reqRealTimeBars(3101, FOPContracts.ES_FOP("201712", 2550, "C", "ES"), 5, "MIDPOINT", True, []) 
  #   self.reqContractDetails(3102, FOPContracts.ES_FOP("201712", 2550, "C", "ES"))
  #   print("realTimeBars: ", FOPContracts.ES_FOP("201712", 2550, "C", "ES"))
  #=============================================================================

  def openOrder(self, orderId: OrderId, contract: Contract, order: Order,
             orderState: OrderState):
#     super().openOrder(orderId, contract, order, orderState)
    print("OpenOrder. ID:", orderId, contract.symbol, contract.secType,
         "@", contract.exchange, ":", order.action, order.orderType,
         order.totalQuantity, orderState.status, contract, order, orderState.maintMargin, orderState.initMargin)
    order.whatIf = True
    self.placeOrder(orderId, contract, order)

  def orderStatus(self, orderId: OrderId, status: str, filled: float,
                    remaining: float, avgFillPrice: float, permId: int,
                    parentId: int, lastFillPrice: float, clientId: int,
                    whyHeld: str): #, mktCapPrice: float):
#         super().orderStatus(orderId, status, filled, remaining,
#                             avgFillPrice, permId, parentId, lastFillPrice, clientId, whyHeld)#, mktCapPrice)
        print("OrderStatus. Id: ", orderId, ", Status: ", status, ", Filled: ", filled,
              ", Remaining: ", remaining, ", AvgFillPrice: ", avgFillPrice,
              ", PermId: ", permId, ", ParentId: ", parentId, ", LastFillPrice: ",
              lastFillPrice, ", ClientId: ", clientId, ", WhyHeld: ",
              whyHeld ) #, ") , MktCapPrice: ", mktCapPrice)
    
  def realtimeBar(self, reqId:TickerId, time:int, open:float, high:float, low:float, close:float, volume:int, wap:float, count:int):
#     super().realtimeBar(reqId, time, open, high, low, close, volume, wap, count)
    print("RealTimeBars. ", reqId, ": time ", time, ", open: ",open, ", high: ", high, ", low: ", low, ", close: ", close, ", volume: ", volume, ", wap: ", wap, ", count: ", count)    

 

  def historicalData(self, reqId:int, bar: BarData):
     self.controller.historicalData.historicalData(reqId, bar) 
 
  def historicalDataEnd(self, reqId: int, start: str, end: str):
     self.controller.historicalData.historicalDataEnd(reqId, start, end) 

  def historicalDataUpdate(self, reqId: int, bar: BarData):
     """Receives bars in real time if keepUpToDate is set as True in reqHistoricalData. """
     self.controller.historicalData.historicalDataUpdate(reqId, bar) 


 
     # ! [managedaccounts]
  def managedAccounts(self, accountsList: str):
    self.account = accountsList.split(",")[0]
    self.controller.managedAccounts(accountsList)
#    self.accountUpdateService = TWSAccountUpdateService()
#    self.reqAccountUpdates(True, self.account)

        
  def contractDetails(self, reqId: int, contractDetails: ContractDetails):
    ########################################################################################################################################
    #if reqId == 221:   ## massenhafte Rückgabe der ausmultiplizierten FOP-Aufrufe der Optionsketten, um die ConIds zu ermitteln (FOP: requestGreekDataFromTWS); wird von PC zum FOP runtergegeben
        ##print("contractDetails 221: ", contractDetails)
    if reqId not in self.controller.productCollection.getProductIds():  
        self.controller.productCollection.setConIdForFOP(reqId, contractDetails) 
        db = IBDatabase()
        if contractDetails.summary.right == "C" or contractDetails.summary.right == "CALL":
            execString = "UPDATE TRADING_CLASS SET TC_CALL_CONID = "  + str(contractDetails.summary.conId) + " WHERE TC_UNDERLYING_CONID = " + str(reqId) +" AND TC_EXCHANGE = '" + str(contractDetails.summary.exchange) +"' AND TC_TRADINGCLASS = '"+str(contractDetails.summary.tradingClass)+"' AND TC_EXPIRATION = '"+str(contractDetails.summary.lastTradeDateOrContractMonth)+"' AND TC_STRIKE = " + str(contractDetails.summary.strike)
        if contractDetails.summary.right == "P" or contractDetails.summary.right == "PUT":
            execString = "UPDATE TRADING_CLASS SET TC_PUT_CONID = "  + str(contractDetails.summary.conId) + " WHERE TC_UNDERLYING_CONID = " + str(reqId) +" AND TC_EXCHANGE = '" + str(contractDetails.summary.exchange) +"' AND TC_TRADINGCLASS = '"+str(contractDetails.summary.tradingClass)+"' AND TC_EXPIRATION = '"+str(contractDetails.summary.lastTradeDateOrContractMonth)+"' AND TC_STRIKE = " + str(contractDetails.summary.strike)
        #print("tws contractDetails: ", execString)
        try:
            db.DBcursor.execute(execString)
            db.DBconnection.commit()
        except sqlite3.Error as e:
            if str(e) != "UNIQUE constraint failed: CONTRACT_DETAILS_FUTURES.FUTURE_CONID":   ## this error simply means that the Future is already in our database
                print("error: ", e)
        db.closeDatabase()
      
    ########################################################################################################################################
    ## hier kommt ein Future zurück, der von einem "requestFuturesForProducts" angefragt wurde; hier können als zu einem Produkt mehrere Futures kommen.
    ## diese Futures werden dann gespeichert; die reqId entspricht dem Produkt 200,201,202 usw.; pro reqId können hier mehrere Futures zurückkommen !
    if reqId in self.controller.productCollection.getProductIds():  
        self.controller.productCollection.addFuture(reqId, contractDetails) 
        db = IBDatabase()
        execString = "INSERT INTO CONTRACT_DETAILS_FUTURES (FUTURE_PRODUCT_ID, FUTURE_CONID, FUTURE_SYMBOL, FUTURE_SECTYPE, FUTURE_LASTTRADEORCONTRACTMONTH, FUTURE_MULTIPLIER, FUTURE_EXCHANGE, FUTURE_CURRENCY) VALUES (" + str(reqId) + ", " + str(contractDetails.summary.conId) + " , '" + contractDetails.summary.symbol + "', '" +  contractDetails.summary.secType + "', '" + contractDetails.summary.lastTradeDateOrContractMonth + "', '" + contractDetails.summary.multiplier + "', '" + contractDetails.summary.exchange + "', '" + contractDetails.summary.currency+ "')" 
        try:
            db.DBcursor.execute(execString)
            db.DBconnection.commit()
        except sqlite3.Error as e:
            if str(e) != "UNIQUE constraint failed: CONTRACT_DETAILS_FUTURES.FUTURE_CONID":   ## this error simply means that the Future is already in our database
                print("error: ", e)
        db.closeDatabase()
    ########################################################################################################################################


  def contractDetailsEnd(self, reqId: int):
    """we are here because all contracts for a reqId are delivered"""
    if reqId in self.controller.productCollection.getProductIds(): 
        a = self.controller.productCollection.getProductForRequestId(reqId) 
        if a != None:
            a.notifyAllFuturesFromTWSareDelivered(self, reqId)
        #print("done")
#     if reqId == 209:
#         self.rootWindow.updateUnderlyingPickerComboboxValue(self.es_optionchain)
    #print("ContractDetailsEnd. ", reqId, "\n")



  def securityDefinitionOptionParameter(self, reqId: int, exchange: str, underlyingConId: int, tradingClass: str, multiplier: str,
          expirations: SetOfString, strikes: SetOfFloat):
    self.controller.productCollection.addOptionChainToFuture(reqId, exchange, underlyingConId, tradingClass, multiplier, expirations, strikes)
    print("tws.py securityDefinitionOptionParameter: ", reqId, exchange, underlyingConId, tradingClass, multiplier, expirations, strikes)
    db = IBDatabase()
    for expiration in expirations:
         for strk in strikes:
             execString = "INSERT INTO TRADING_CLASS (TC_UNDERLYING_CONID, TC_EXCHANGE, TC_TRADINGCLASS, TC_EXPIRATION, TC_STRIKE, TC_CALL_CONID, TC_PUT_CONID) VALUES (" + str(reqId) + ", '" + exchange + "' , '" + tradingClass + "', '" +  expiration + "', '" + str(strk) +  "', NULL, NULL)" 
             try:
                 db.DBcursor.execute(execString)
                 db.DBconnection.commit()
             except sqlite3.Error as e:
                 if str(e) != "UNIQUE constraint failed: TRADING_CLASS.TC_UNDERLYING_CONID, TRADING_CLASS.TC_EXCHANGE, TRADING_CLASS.TC_TRADINGCLASS, TRADING_CLASS.TC_EXPIRATION, TRADING_CLASS.TC_STRIKE":   ## this error simply means that the Future is already in our database
                     print("error: ", e)
    db.closeDatabase()
    #===========================================================================
    # for expiration in expirations:
    #             self.loadFOP(reqId, exchange, underlyingConId, tradingClass, multiplier, expiration, "")
    #===========================================================================
    

  #=============================================================================
  # def loadFOP(self, reqId, exchange, underlyingConId, tradingClass, multiplier, expiration, strk):
  #       uci = self.controller.productCollection.getFutureByConId(underlyingConId)
  #       co = Contract()
  #       co.symbol = uci.summary.symbol
  #       co.strike = strk
  #       co.exchange = exchange
  #       co.multiplier = multiplier
  #       co.lastTradeDateOrContractMonth = expiration
  #       co.right = "C"
  #       co.secType = "FOP"
  #       co.currency = uci.summary.currency
  #       co.tradingClass = tradingClass
  #       self.reqContractDetails(underlyingConId, co)
  #       print("loadFOP: ", co)
  #       
  #       po = Contract()
  #       po.symbol = uci.summary.symbol
  #       po.strike = strk
  #       po.exchange = exchange
  #       po.multiplier = multiplier
  #       po.lastTradeDateOrContractMonth = expiration
  #       po.right = "P"
  #       po.secType = "FOP"
  #       po.currency = uci.summary.currency
  #       po.tradingClass = tradingClass
  #       self.reqContractDetails(underlyingConId, po)   
  #       print("loadFOP: ", po)     
  #=============================================================================
    
  def securityDefinitionOptionParameterEnd(self, reqId: int):
      """secDefOptParamEnd: alle Optionsketten-Elemente sind angekommen
      
        .. uml::  
   
           @startuml 
              title Befüllen der Combobox

                TWSWrapper -> Controller: securityDefinitionOptionParameterEnd
                Controller -> ProductCollection: notifyAllOptionChainsAreDeliveredForReqId

                TWSWrapper -> Controller: getProductsAndFuturesWithExistingTradingClassesAsFormattedList
                ProductCollection --> Product: getAllFuturesWithExistingTradingClassesAsFormattedList
                Product --> Future: getFutureWithExistingTradingClassesAsFormattedList
                Future --> Future: getTradingClasses - return liste
                
           @enduml
      
      """
      self.controller.securityDefinitionOptionParameterEnd(reqId) 
      self.rootWindow.updateUnderlyingPickerComboboxValue(self.controller.productCollection.getProductsAndFuturesWithExistingTradingClassesAsFormattedList())    
      ## nachdem ich den reqId am Wickel habe (also did ConId vom Future und alles da sein müsste, kann man nun die ContractDetails aller Optionsketten Strikes holen


 
 ###############################################################################################################
 
class TWSAccountUpdateService(Observable):
    def __init__(self, controller, account):
        self.controller = controller
        self.observers = []
        self.accountPositions = []    
        self.account = account
        
        self.portfolioObserver = Observable()
        self.accountValueObserver = Observable()
    
    ### Portfolio Observer ###    
    def updatePortfolio(self, contract: Contract, position: float,
                      marketPrice: float, marketValue: float,
                      averageCost: float, unrealizedPNL: float,
                      realizedPNL: float, accountName: str):
        print("TWSAccountUpdateService: UpdatePortfolio.", contract.conId, " ",contract.symbol, "", contract.secType, "@",
                contract.exchange, "Position:", position, "MarketPrice:", marketPrice,
                "MarketValue:", marketValue, "AverageCost:", averageCost,
                "UnrealizedPNL:", unrealizedPNL, "RealizedPNL:", realizedPNL,
                "AccountName:", accountName)    
        thelist = [d for d in self.accountPositions if d.get('conId') != contract.conId]
        self.accountPositions = thelist
        if contract.secType == 'FOP':
            self.accountPositions.append({
                                        'conId':contract.conId,
                                        'contract':contract, 
                                        'position':position, 
                                        'marketPrice':marketPrice, 
                                        'marketValue':marketValue,
                                        'averageCost':averageCost,
                                        'unrealizedPNL':unrealizedPNL,
                                        'realizedPNL':realizedPNL,
                                        'accountName':accountName,
                                        'lastTradeDateOrContractMonth':contract.lastTradeDateOrContractMonth
                                        })
        self.controller.view.rootWindow.updateTreeItem(self.accountPositions)
        wert = 0
        cost = 0
        for a in self.accountPositions:
            if a['contract'].secType == 'FOP':
                wert = wert + a['unrealizedPNL']
                cost = cost + a['averageCost']
        self.controller.view.rootWindow.appendChart(strftime("%H:%M:%S", time.localtime()), wert, cost)
        self.controller.view.rootWindow.updateProfitButton('% 3.2f' % wert)
        self.controller.view.rootWindow.updateMaxProfitButton('% 3.2f' % cost)
        self.controller.view.rootWindow.updateRiskQuoteButton('% 3.2f %%' % ((cost / float(self.controller.view.rootWindow.getNetLiquidationLabel())) * 100) )
        self.updatePortfolioObservers(self.accountPositions)

    def position(self, account, contract, position, avgCost):
        print("Position.", account, "Symbol:", contract.symbol, "SecType:",
                contract.secType, "Currency:", contract.currency,
                "Position:", position, "Avg cost:", avgCost)
    
    def positionEnd(self):
        print("PositionEnd")
        self.updatePortfolioObservers(self.accountPositions)

    def updatePortfolioObservers(self, accountPositions):
        for observer in self.portfolioObserver.observers:
            observer.portfolioChanged(accountPositions)
    
    def getPositions(self, aSymbol):
        positionList = []
        for a in self.accountPositions:
            if a['contract'].symbol == aSymbol:
                positionList.append(a)
        return positionList
    
    ### Account Value Observer ###    
    def updateAccountValue(self, key: str, val: str, currency: str,   accountName: str):
        ##print("TWSAccountUpdateService: ", accountName, key, val, currency)
        if key == 'LookAheadMaintMarginReq':
            self.controller.view.rootWindow.updateLookAheadMarginLabel(val)        
            self.LookAheadMaintMarginReq = float(val)
            print('LookAheadMaintMarginReq: ', self.LookAheadMaintMarginReq)
            if hasattr(self, 'NetLiquidation'):
                self.printAccountMarginQuote()
        if key == 'NetLiquidation':
            self.controller.view.rootWindow.updateNetLiquidationLabel(val)
            self.NetLiquidation = float(val)
            print('NetLiquidation: ', self.NetLiquidation)
            if hasattr(self, 'LookAheadMaintMarginReq'):
                self.printAccountMarginQuote()

    def printAccountMarginQuote(self):
        print("Margin Quote = % 3.2f %%" % (self.LookAheadMaintMarginReq / self.NetLiquidation * 100))
        self.controller.view.rootWindow.updateQuote("% 3.2f %%" % (self.LookAheadMaintMarginReq / self.NetLiquidation * 100))
        
    def updateAccountTime(self):
        pass
    
    def accountDownloadEnd(self, accountName: str):
        print("TWSAccountUpdateService: - Account download finished:", accountName)
        self.updateAccountValueObservers(accountName)
        
    def updateAccountValueObservers(self, accountName:str):
        for observer in self.accountValueObserver.observers:
            observer.accountValueChanged(accountName)
    
