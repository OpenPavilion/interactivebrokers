#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Model Module

.. inheritance-diagram:: model
   :parts: 2

"""

import sys

from ibapi.contract import *
from ibapi import *
from ibapi.common import *
import pickle, dill
from observer import *

import datetime
from prompt_toolkit.key_binding.bindings.named_commands import redraw_current_line

# ###############################################################################################################
# 
# class OptionTrading_MarketData_Throttle:
#      """
#      OptionTrading_MarketData_Throttle koordiniert das Laden der Furure-Optionen mit einer Begrenzung auf 100 aktive Aufrufe 
#      """
#      def __init__(self):
#          pass   ## TODO lade 100er weise
# 
###############################################################################################################

class OptionTrading_Product_Collection:
    """
    OptionTrading_Basiswert_Collection ist der Einstieg zu den Basiswerten, Futures, Handelsklassen 
    und Future-Optionen für ES, CL, EUR ... usw.
     
    """
    def __init__(self):
        """This function initializes the object for OptionTrading_Product_Collection.""" 
        self.products = []
        self._nextProductId = 0

    def pickleDump(self):
        #pickle.dump(self._nextProductId, open("save.data","wb"))
        pickle.dump(self, open("save.data","wb"))
        with open("save.data", 'r', errors='replace') as f:
            print(f.readlines())
        
    def pickleLoad(self):
        pickle.load(open("save.data","rb"))
        with open("save.data", 'r', errors='replace') as f:
            print(f.readlines())
        
    def addProduct(self, aOptionTrading_Product):
        aOptionTrading_Product.setParent(self)  ## link to parent
        self.products.append(aOptionTrading_Product) 
        ## TODO: check for double entries in the list
        # Lösung siehe unten ??
        #   https://stackoverflow.com/questions/9427163/remove-duplicate-dict-in-list-in-python
        #   https://stackoverflow.com/questions/8972076/remove-duplicates-from-the-list-of-dictionaries

    def setRootFrame(self, rf):
        self.rootFrame = rf  ## REFACTOR STEP #020: self.rootFrame macht keinen Sinn im Model; die Methoden müssen alle in den Controller !
    
    def getNextProductId(self):
        self._nextProductId += 1
        return self._nextProductId
    
    def getProductIds(self):
        liste = []
        for a in self.getProducts():
            liste.append(a.getRequestId())
        return liste
    
    def getProducts(self):
        return self.products
    
    def getProductForRequestId(self, reqId):
        for p in self.getProducts():
            if p.getRequestId() == reqId:
                return p
        return None

    def getProductBySymbol(self, aSymbol):
        for p in self.getProducts():
            if p.getSymbol() == aSymbol:
                return p
        return None

    def getFutureByConId(self, underlyingConId):
        for p in self.getProducts():
            result = p.getFutureByConId(underlyingConId)
            if result != None:
                return result
        return None
    
    def addFuture(self, reqId, contractDetails):
        """hier kommt ein Future zurück aus dem Baustein ContractDetails im tws.py"""
        for a in self.getProducts():
            if reqId == a.getRequestId():
                a.addFuture(OptionTrading_Future(a, contractDetails))
                
    def addOptionChainToFuture(self, reqId, exchange, underlyingConId, tradingClass, multiplier, expirations, strikes):
        for p in self.getProducts():
            for f in p.getFutures():
                if f.getConId()  == reqId:
                    f.addOptionChainToFuture( reqId, exchange, underlyingConId, tradingClass, multiplier, expirations, strikes )
    
    def requestFuturesForAllProducts(self, aTWSWrapper):
        """step through all products and request data"""
        for p in self.getProducts():
            p.requestFuturesForSingleProduct(aTWSWrapper)   ## iterate through all Products, get Futures and load their OptionChains 
            
    def notifyAllOptionChainsAreDeliveredForReqId(self, reqId, aTWSWrapper):
        #print("notifyAllOptionChainsAreDeliveredForReqId: ", reqId)  ## <<-- reqId ist eine Future ConId   (Product-Schleife über alle Futures --> notifyAllFuturesFromTWSareDelivered)
        self.rootFrame.updateUnderlyingPickerComboboxValue(["Aktualisiere Handelklassen ..."])
        ## REFACTOR STEP #020: self.rootFrame macht keinen Sinn im Model; die Methoden müssen alle in den Controller !
        for p in self.getProducts():
            for f in p.getFutures():
                if f.getConId()  == reqId:
                    f.notifyAllOptionChainsAreDelivered(reqId, aTWSWrapper)

    def triggerTWStoFetchFutureHistoricalDataAndOptionChainGreeks(self, value, aController, aStockChart):
        for p in self.getProducts():
            p.triggerTWStoFetchFutureHistoricalDataAndOptionChainGreeks(value, aController, aStockChart)
            
    #===========================================================================
    # def getFutureTradingClassAndExpiration(self):
    #     liste = []
    #     for p in self.getProducts():
    #         gf = p.getFutureTradingClassAndExpiration()
    #         if gf != []:
    #             liste += gf
    #     return liste
    #===========================================================================

    def getProductsAndFuturesWithExistingTradingClassesAsFormattedList(self):
        """
        drill down into all products and futures and check for all futures, that have existing TradingClasses,
        which indicate that OptionChains are available
        this result (list) will be used for the selection combobox in the view to select the contract
        """
        liste = []
        for p in self.getProducts():
            gf = p.getAllFuturesWithExistingTradingClassesAsFormattedList()
            if gf != []:
                liste += gf
        return liste  
    
    def setConIdForFOP(self, reqId, contractDetails):
        for p in self.getProducts():
            p.setConIdForFOP(reqId, contractDetails)
        # SI FOP ConId: 119856647 @ NYMEX NYMEX Silver Index 201712 76.0 P SOZ7 P7600 
       

###############################################################################################################
class OptionTrading_Product:
    """
    Basiswert (ES, FUT, GLOBEX, USD, 50) - der Einstieg für Futures, Handelsklassen und Strikes
    
    Dieses Element selbst hat keine ConId, weil es nur die Klammer ist für Futures und Optionsketten.
    """
    def __init__(self, aOptionTrading_Product_Collection,aReqId,  aSymbol, aSecType, aExchange, aCurrency, aMultiplier, aCotData ):
        self.futures = [] ## empty Futures Dictionary by default
        self.parentCollection = aOptionTrading_Product_Collection
        self.requestId = aReqId  ## take the reqId from the SQLite database (i.e. ID_PRODUCT of Product in Database)
        self.setContract(aSymbol, aSecType, aExchange, aCurrency, aMultiplier)  
        self.cot = aCotData
        self.symbol = aSymbol
        
    def setParent(self, aParent):
        self.parent = aParent   
    
    def getParent(self):
        return self.parent
    
    def get_cot_data(self):
        return self.cot
    
    def getSymbol(self):
        return self.symbol
    
    def setContract(self, aSymbol, aSecType, aExchange, aCurrency, aMultiplier):   
        self.contract = Contract() ## create base contract
        self.contract.symbol = aSymbol ## e.g. "ES"
        self.contract.secType = aSecType ## e.g. "FUT"
        self.contract.exchange = aExchange ## e.g. "GLOBEX"
        self.contract.currency = aCurrency ## e.g. "USD"
        self.contract.multiplier = aMultiplier ## e.g. "50"

    def setVolatilityProduct(self, aVolatilityProduct):
        self.volatilityProduct = aVolatilityProduct
        self.volatilityProduct.setParentProduct(self)

    def getVolatilityProduct(self):
         return self.volatilityProduct

    def getContract(self):
        return self.contract
    
    def getRequestId(self):
        return self.requestId
    
    def getParentCollection(self):
        return self.parentCollection
    
    def getDescription(self):
        return self.description
    
    def getProductRequestId(self):
        return self.requestId
    
    def getFutures(self):
        return self.futures
    
    def setConIdForFOP(self, reqId, contractDetails):
        for f in self.getFutures():
            if f.getContractDetails().summary.symbol == contractDetails.summary.symbol:
                f.setConIdForFOP(reqId, contractDetails)
                
    def getFutureByConId(self, underlyingConId):
        for f in self.getFutures():
            fut = f.getContractDetails()
            if fut.summary.conId == underlyingConId:
                return(fut)
        return None


    def triggerTWStoFetchFutureHistoricalDataAndOptionChainGreeks(self, value, aController, aStockChart):
        bSymbol = value.partition("/")[0]  ## ES, CL ...
        bDate = value.split('/')[1].split(' ')[0] ##  20180522 ...
        for f in self.getFutures():
            aSymbol = f.getFutureSymbol()
            aDate = f.getContractDetails().summary.lastTradeDateOrContractMonth
            if aSymbol == bSymbol and aDate == bDate:   ## passenden Future finden
                f.register(aStockChart)  ## register Chart as receiver
                ######## f.requestConIdsForOptionChainFOPs(aController.tws)  ## erst mal die FOPs der Optionsketten zum Future erzeugen und die ConIds holen
                queryTime = datetime.datetime.today().strftime("%Y%m%d %H:%M:%S")
                print("model.py - triggerTWStoFetchFutureHistoricalDataAndOptionChainGreeks: ",f.getContractDetails().summary.conId, f.getContractDetails().summary, queryTime)
                aController.historicalData.appendFutureObserver(f.getContractDetails().summary.conId, f)
                aController.tws.reqHistoricalData(f.getContractDetails().summary.conId, f.getContractDetails().summary, queryTime, 
                    "12 M", "1 day", "MIDPOINT", 0, 1,  False, []) ## historicalData zum Future laden


    
    #===========================================================================
    # def getFutureTradingClassAndExpiration(self):
    #     liste = []
    #     for f in self.getFutures():
    #         liste += f.getTradingClassAndExpiration()
    #     return liste
    #===========================================================================
    
    def getAllFuturesWithExistingTradingClassesAsFormattedList(self):
        liste = []
        for f in self.getFutures():
            liste += f.getFutureWithExistingTradingClassesAsFormattedList()
        return liste        
        
    def addFuture(self, aOptionTrading_Future):  ## returned by contractDetails()
        self.futures.append(aOptionTrading_Future)  ## Expiration Date of Future such as 201712, 201803 ...
        ## TODO: check for double entries in the list
        
   
    def requestFuturesForSingleProduct(self, aTWSWrapper):
        """load ContractDetails for this Product; since a product is not unique, as a result we should get a list of future contracts in response in ContractDetails methode in tws.py """
        ## TODO: aktuell wird die Nummer 200 hochgezählt als reqId im ContractDetails  <<-- schlechter Design !
        aTWSWrapper.reqContractDetails(self.getProductRequestId(), self.getContract())
        ########################################################
        ## von hier aus muss man den Future nach unten durchlaufen und die Strikes abfragen
        ## wichtig ist, dass die reqMktData immer 100-er weise stattfinden
        ########################################################
        pass ## TODO reqContractDetails and receive all Futures, so that we have a list of currently valid futures for the product
    
    def notifyAllFuturesFromTWSareDelivered(self, aTWSWrapper, reqId):
        """we have received all futures for this product, so we can now fetch the option chains for all futures"""
        #print("notifyAllFuturesFromTWSareDelivered: ", reqId)  ## being called from TWS Wrapper; indicates that the delivery of the Futures is finished, so the next action can start (e.g. request OptionChains)
        self.fetchOptionChains(aTWSWrapper) ## <<-- now we can load all OptionChains for All Futures of This Product

    def fetchOptionChains(self, aTWSWrapper):
        for f in self.getFutures():
            a = f.getContractDetails()
            #print("model.py - fetchOptionChains: ", f.getContractDetails())
            aTWSWrapper.reqSecDefOptParams(a.summary.conId, a.summary.symbol, a.summary.exchange, a.summary.secType, a.summary.conId)

###############################################################################################################

class OptionTrading_VolatilityProduct:

    def __init__(self, aContractDetails):
        self.marketData = [] ## empty marketData Dictionary
        self.contractDetails = aContractDetails   
        self.bars = []
        
    def getContractDetails(self):
        return self.contractDetails
    
    def getConId(self):
        return self.contractDetails.summary.conId        
                     
    def requestMarketDataFromTWS(self):
        pass ## TODO reqMktData from TWS and save it in this future   
    
    def setParentProduct(self, aParent):
        self.parent = aParent
    
    def getParentProduct(self):
        return self.parent  
    
    def addBar(self, aBar):
        self.bars.append(aBar)
    
    def getBars(self):
        return self.bars
    
    def requestHistoricalData(self):
        self.getParentProduct().getParent().requestHistoricalData(self) 
        ## REFACTOR STEP #020: 2x getParent(), wer is denn das überhaupt ??
        
    def historicalDataEnd(self):
        self.getParentProduct().getParent().rootFrame.redrawVolatilityProduct(self) 
        ## REFACTOR STEP #020: muss in den Controller verlagert werden
        ## wird aufgerufen von: 

###############################################################################################################
class OptionTrading_Future(Observable):
    """
    OptionTrading_Future gibt es periodisch immer wieder neu zu einem Basiswert. Sie haben eine begrenzte Laufzeit und laufen aus.
    Es gibt monatliche, quartalsweise ... Laufzeiten, die bis zu einigen Jahren in der Zukunft liegen.
    Zu den Futures wiederum gibt es Handelklassen, die wöchentlich, monatlich, quartalsweise sein können und dazu gehörige Strikes,
    die dann zu den Future-Optionen führen, die wir handeln wollen.
    Futures werden ermittelt über reqContractDetails eines Basiswertes. Es werden so viele Futures zurückgegeben in contractDetails,
    wie derzeit aktiv gehandelt werden. 
    
    Zu einem Future können folgende Dinge geladen werden:
    
    - Marktdaten des Futures selbst (reqMktData, realtime/latest)
    - tradingClasses und deren Strikes (reqSecDefOptParams)
    
    """
    def __init__(self, aProduct, aContractDetails):
        self.product = aProduct
        self.tradingClasses = [] ## empty Futures Dictionary
        self.marketData = [] ## empty marketData Dictionary
        self.contractDetails = aContractDetails
        self.bars = []
        self.observers = []
     
    def getContractDetails(self):
        return self.contractDetails
    
    def getConId(self):
        return self.contractDetails.summary.conId
    
    def getTradingClasses(self):
        return self.tradingClasses
    
    def getFutureSymbol(self):
        return self.contractDetails.summary.symbol

    def addBar(self, aBar):
        self.bars.append(aBar)
    
    def getBars(self):
        return self.bars
    
    def getProduct(self):
        return self.product

    def historicalDataEnd(self):
        self.update_observers(self.getBars(), self.getProduct())
        pass ## Controller is finished with loading HistoricalData for this Future
     
    def setConIdForFOP(self, reqId, contractDetails):
        for t in self.getTradingClasses():
            if t.getTradingClass() == contractDetails.summary.tradingClass:
                t.setConIdForFOP(reqId, contractDetails)

    def requestConIdsForOptionChainFOPs(self, aTWSWrapper):
        for tc in self.getTradingClasses():
            print("model.py - requestConIdsForOptionChainFOPs: ", self.getFutureSymbol())
            ###########################
            ##  TODO: unschöner Code "requestConIdsForOptionChainFOPs"
            ##  hier werden zuerst zu den OptionChains die ConId geholt und dann sollen die griechen geladen werden (Griechen laden 
            ##  ist noch nicht implementiert
            ##
            print("setFutureOptionsInAllStrikes: ",self.contractDetails)
            tc.setFutureOptionsInAllStrikes(aTWSWrapper, self.contractDetails) ## setze in allen Optionsketten ein Contract-Objekt für Put und Call
            print("requestConIdsForOptionChainFOPs")
            tc.requestConIdsForOptionChainFOPs(aTWSWrapper) ## hole erst mal alle conIds zu den FOPs der OptionChain
            print("tc.holeWirklichDieGriechen(....) fehlt !!! bisher werden nur die Optionsketten mit ConIds gefüllt")
            for x in self.getTradingClasses():  
                for y in x.getStrikes():
                    print("Call/Put Option: ", self.contractDetails.summary.symbol," " , self.contractDetails.summary.lastTradeDateOrContractMonth, " Class: " , x.getTradingClass(), " Strike: ", y.getStrike(), " Optionen: ", y.getCallOption(), y.getPutOption())

            ## tc.holeWirklichDieGriechen(....)
            ##
            ## die "requestConIdsForOptionChainFOPs" Methode holt fälschlicherweise nur die ConIds, 
            ## aber nicht die Marktdaten; das fehlt !!!!!
            ## 
            ## Frage:
            ## kann man das Füllen der Optionsketten nicht schon dann machen, wenn die Optionsketten angelegt werden !?!
            ################################
    
    #===========================================================================
    # def getTradingClassAndExpiration(self):
    #     liste = []
    #     for t in self.getTradingClasses():
    #         liste.append( str(self.getFutureSymbol())+"/"+t.getTradingClass()+"/"+t.getExpirationDate()[:4] + "-" +  t.getExpirationDate()[4:6] + "-"+t.getExpirationDate()[6:8] + " (" + datetime.date(int(t.getExpirationDate()[:4]),int(t.getExpirationDate()[4:6]),int(t.getExpirationDate()[6:8])).strftime("%A")[:3]+")")
    #     return liste
    #===========================================================================
    
    def getFutureWithExistingTradingClassesAsFormattedList(self):
        liste = []
        if self.getTradingClasses() != []:
            liste.append(self.getFutureSymbol() + "/"+self.getContractDetails().summary.lastTradeDateOrContractMonth+" ("+str(len(self.getTradingClasses()))+")")
            #===================================================================
            # for t in self.getTradingClasses():
            #     liste.append( str("... "+self.getFutureSymbol())+"/"+t.getTradingClass()+"/"+t.getExpirationDate()[:4] + "-" +  t.getExpirationDate()[4:6] + "-"+t.getExpirationDate()[6:8] + " (" + datetime.date(int(t.getExpirationDate()[:4]),int(t.getExpirationDate()[4:6]),int(t.getExpirationDate()[6:8])).strftime("%A")[:3]+")")
            #===================================================================
        return liste
    
    def requestMarketDataFromTWS(self):
        pass ## TODO reqMktData from TWS and save it in this future

    def add_FUT_MarketData(self, aOptionTrading_FUT_MarketData):
        self.marketData.append(aOptionTrading_FUT_MarketData)
        ## TODO: check for double entries in the list

    def addOptionChainToFuture(self, futureConId, exchange, underlyingConId, tradingClass, multiplier, expirations, strikes):  ## reqId = futureConId
        for expiration in expirations:
            self.addTradingClass( OptionTrading_TradingClass(tradingClass, expiration, strikes, futureConId))
    
    def fetchTradingClassesFromTWS(self):
        pass ## TODO reqSecDefOptParams and receive all currently available tradingClasses for this future

    def addTradingClass(self, aTradingClass):
        self.tradingClasses.append(aTradingClass)
        ## TODO: check for double entries in the list

    def notifyAllOptionChainsAreDelivered(self, reqId, aTWSWrapper):  
        pass ##print(".... notifyAllOptionChainsAreDelivered: ", reqId, self.contractDetails, self.tradingClasses)
        ### -->> JETZT MÜSSEN WIR DIE CONIDs LADEN ZU DEN OPTIONSKETTEN  <<--
        #=======================================================================
        # print("........ Beginn requestConIdsForOptionChainFOPs")
        # print("........ Ende requestConIdsForOptionChainFOPs")
        #=======================================================================
        

###############################################################################################################

class OptionTrading_TradingClass:
    """
    Zu jedem Future gibt es eine oder mehrere Handelsklassen, die ihrerseits Strikes beinhalten, die zu den Future-Optionen führen.
    Eine Handelklasse können WEEKLY, MONTHLY, QUARTERTLY sein e.g. ES: ES, E1A, E1C, E2C, E5A, EW, EW1, EW2, EW3, E1C usw.
    """
    def __init__(self, aTradingClass, expirationDate, strikes, futureConId):
        self.strikes = []
        self.tradingClass = aTradingClass  ## e.g. ES, E1A, E1C, E2C, E5A, EW, EW1, EW2, EW3, E1C usw.
        self.expirationDate = expirationDate
        for strk in sorted(strikes):
            self.strikes.append( OptionTrading_Strike(strk, futureConId) )

    def addStrike(self, aOptionTrading_Strike):
        self.strikes.append(aOptionTrading_Strike)
        ## TODO: check for double entries in the list
    
    def getStrikes(self):
        return self.strikes
    
    def getTradingClass(self):
        return self.tradingClass
    
    def getExpirationDate(self):
        return str(self.expirationDate)
     
    def setConIdForFOP(self, reqId, contractDetails):
        for strk in self.getStrikes():
            if strk.getStrike() == contractDetails.summary.strike:
                strk.setConIdForFOP(reqId, contractDetails)
    
    def setFutureOptionsInAllStrikes(self, aTWSWrapper, aFutureContract):
        for strk in self.getStrikes():
            strk.setFutureOptionsInAllStrikes(aTWSWrapper, aFutureContract, self.getTradingClass(), self.getExpirationDate())

    def requestConIdsForOptionChainFOPs(self, aTWSWrapper):
        print("requestConIdsForOptionChainFOPs: ",self.tradingClass+"/"+self.expirationDate)
        for strk in self.getStrikes():
            #print(strk)
            strk.requestConIdsForOptionChainFOPs(aTWSWrapper)
            ## trigger reqMktData für alle Optionsketten

###############################################################################################################
class OptionTrading_Strike:
    """
    OptionTrading_Strike ist ein Sammler für die beiden Future-Optionen Call/Put pro Strike
    
    Wichtige Funktionen:
    - requestOptionsFromTWS :  Man kann vom Strike aus die TWS anfragen und sich die beiden FOPs geben lassen
    """
    def __init__(self, strike, futureConId):
        self.callOption = None ## init value
        self.putOption = None  ## init value
        self.strike = strike ## no set method, this can't be changed anymore after initialization
        self.futureConId = futureConId
        
    def getStrike(self):
        return self.strike
    
    def getCallOption(self):
        return self.callOption
    
    def getPutOption(self):
        return self.putOption
    
    def setCallOption(self, aOptionTrading_FOP):
        self.callOption = aOptionTrading_FOP

    def setPutOption(self, aOptionTrading_FOP):
        self.putOption = aOptionTrading_FOP
        
    def setConIdForFOP(self, reqId, contractDetails):
        if (contractDetails.summary.right == "C" or contractDetails.summary.right == "CALL") and self.getCallOption() != None:
            self.getCallOption().setConIdForFOP(reqId, contractDetails)
            
        if (contractDetails.summary.right == "P" or contractDetails.summary.right == "PUT" ) and self.getPutOption() != None:
            self.getPutOption().setConIdForFOP(reqId, contractDetails)          
        
    def setFutureOptionsInAllStrikes(self, aTWSWrapper, aFutureContract, aTradingClass, anExpirationDate):
        ##print("model.py - setFutureOptionsInAllStrikes: ", aFutureContract.summary)
        po = OptionTrading_FOP(Contract(), self.futureConId)
##        print("setFutureOptionsInAllStrikes : ", putOption.getContract().summary.symbol, putOption.getContract().summary.strike, putOption.getContract().summary.conId, putOption.getContract().summary.right, putOption.getContract().summary.exchange)
        po.getContract().symbol = aFutureContract.summary.symbol
        po.getContract().strike = self.getStrike()
        po.getContract().exchange = aFutureContract.summary.exchange
        po.getContract().multiplier = aFutureContract.summary.multiplier
        #po.getContract().lastTradeDateOrContractMonth = aFutureContract.summary.lastTradeDateOrContractMonth ### [0:6]   ## Jahr + Monat, kein Tag
        po.getContract().lastTradeDateOrContractMonth = anExpirationDate    #### TODO <<-- unklar was ich nehmen soll: Datum vom Future-Ende, Datum von der Trading-Class ?
        po.getContract().right = "P"
        po.getContract().secType = "FOP"
        po.getContract().currency = aFutureContract.summary.currency
        po.getContract().tradingClass = aTradingClass
        #po.getContract().localSymbol = aFutureContract.summary.localSymbol
        self.setPutOption(po)
        ##print("setFutureOptionsInAllStrikes putOption: ", self.getPutOption().getContract())

        co = OptionTrading_FOP(Contract(), self.futureConId)
        co.getContract().symbol = aFutureContract.summary.symbol
        co.getContract().strike = self.getStrike()
        co.getContract().exchange = aFutureContract.summary.exchange
        co.getContract().multiplier = aFutureContract.summary.multiplier
        #co.getContract().lastTradeDateOrContractMonth = aFutureContract.summary.lastTradeDateOrContractMonth ###[0:6]  ## Jahr + Monat, kein Tag#
        co.getContract().lastTradeDateOrContractMonth = anExpirationDate #### TODO <<-- unklar was ich nehmen soll: Datum vom Future-Ende, Datum von der Trading-Class ?
        co.getContract().right = "C"
        co.getContract().secType = "FOP"
        co.getContract().currency = aFutureContract.summary.currency
        co.getContract().tradingClass = aTradingClass
        #co.getContract().localSymbol = aFutureContract.summary.localSymbol
        self.setCallOption(co)
        ##print("setFutureOptionsInAllStrikes callOption: ", self.getCallOption().getContract())

        
    def requestConIdsForOptionChainFOPs(self, aTWSWrapper):
        ##print("model.py - requestConIdsForOptionChainFOPs: ", self.getStrike(), " - Contract Call: ", self.getCallOption().getContract(), " ### Contract Put: ", self.getPutOption().getContract())
        self.getCallOption().requestConIdFromTWS(aTWSWrapper)
        self.getPutOption().requestConIdFromTWS(aTWSWrapper)

        ################################################################################
        ## die Daten werden mit dem POPULATE geholt
        ## self.getCallOption()   
        ## self.getPutOption()
        ################################################################################
    
    
###############################################################################################################    
class OptionTrading_FOP:
    """
    OptionTrading_FOP ist eine Future Option auf einem Strike in einer Handelsklasse.
    Es gibt zu jedem Strike immer eine CALL und eine PUT Option.
    Jede FOP kann greekData haben und auch marketData (Bid/Ask)
    """
    def __init__(self, aContract, futureConId):
        self.greekData = []  ## init value
        self.contract = aContract   ## no set method, this can't be changed anymore after initialization
        self.FOP_REQUEST_ID = 221  ## DUMMY Request Id, since we don't have a ConId yet
        self.futureConId = futureConId
    
    def getContract(self):
        return self.contract
    
    def getGreekData(self):
        return self.greekData
    
    def setConIdForFOP(self, reqId, contractDetails):
        if reqId == self.FOP_REQUEST_ID:
            ##print("setConIdForFOP: ", reqId, contractDetails.summary)
            self.getContract().conId = contractDetails.summary.conId
            print("FOP conId found: ", self.getContract().conId, " summary:", self.getContract())

    def requestConIdFromTWS(self, aTWSWrapper):
        print("requestConIdFromTWS: ", self.futureConId, self.getContract())
        aTWSWrapper.reqContractDetails(self.futureConId, self.getContract())
        #### aTWSWrapper.reqMktData(1013, self.getContract(), "", True, False, [] ) ## ==> tickOptionComputation
        
        ### https://interactivebrokers.github.io/tws-api/md_request.html#gsc.tab=0 
        ### By invoking the IBApi::EClient::reqMktData function passing in true for the snapshot parameter, 
        ### the client application will receive the currently available market data once before a 
        ### IBApi.EWrapper.tickSnapshotEnd event is sent 11 seconds later. Snapshot requests can only be made 
        ### for the default tick types; no generic ticks can be specified. It is important to note that a 
        ### snapshot request will only return available data over the 11 second span; in many cases values 
        ### for all default ticks will not be returned. However snapshots are allowed a higher rate limit than 
        ### the normal 50 messages per second limitation of other EClientSocket functions.


    def addGreekData(self, aOptionTrading_FOP_GreekData):
        self.greekData.append(aOptionTrading_FOP_GreekData)
        ## TODO: check for double entries in the list

###############################################################################################################
class OptionTrading_FOP_GreekData:
    """
    OptionTrading_FOP_GreekData ist ein reines Datenobjekt mit einem GREEK Datensatz zu einem Zeitpunkt.
    """
    def __init__(self, timeStamp, greekData):
        self.timestamp = timeStamp
        self.greekData = greekData
    
    def getTimeStamp(self):
        return self.timestamp

    def getGreekData(self):
        return self.greekData


###############################################################################################################
class OptionTrading_FUT_MarketData:
    """
    OptionTrading_FUT_MarketData ist ein reines Datenobjekt mit einem MARKET Datensatz zu einem Zeitpunkt.
    mit reqMktData kann man Marktdaten von Futures und Future-Optionen abfragen.
    Diese Ergebnisse werden in dem Objekt OptionTrading_FUT_MarketData gespeichert, das über einen timestamp identifiziert werden kann.
    """
    def __init__(self, timeStamp, marketData):
        self.timestamp = timeStamp
        self.marketData = marketData
 
    def getTimeStamp(self):
        return self.timestamp

    def getMarketData(self):
        return self.marketData

###############################################################################################################

class OptionTrading_Position:
    def __init__(self):
        pass



 











###############################################################################################################

class OptionChain:
    def __init__(self, symbol, secType, exchange, currency, multiplier):
        self.futures = [] ## empty Futures Dictionary
        self.options = [] ## empty Options Dictionary
        self.tradingClasses = []
        self.contract = Contract() ## create base contract
        self.contract.symbol = symbol ## e.g. "ES"
        self.contract.secType = secType ## e.g. "FUT"
        self.contract.exchange = exchange ## e.g. "GLOBEX"
        self.contract.currency = currency ## e.g. "USD"
        self.contract.multiplier = multiplier ## e.g. "50"
    
    def getBaseContract(self):
        return self.contract ## return a base contract
        
    def addFuture(self, expirationDate, tradingClass, contractDetails):  ## returned by contractDetails()
        self.futures.append({'expirationDate':expirationDate, 'tradingClass':tradingClass, 'contractDetails':contractDetails})  ## Expiration Date of Future such as 201712, 201803 ...
    
    def getAllFutures(self):
        return self.futures
    
    def addTradingClass(self, tradingClass, expiration, strikes):
        self.tradingClasses.append({'tradingClass':tradingClass, 'expiration':expiration, 'strikes':strikes})
    
    def getTradingClasses(self):
        return self.tradingClasses
    
    def setStrikes(self, value):
        self.strikes = value
    
    def getStrikes(self):
        return self.strikes

    def addOption(self, expirationDate, tradingClass, optionStrike, optionRight, contractDetails, rid):
        ex = 0
        for a in self.options:
            if a['expirationDate'] == expirationDate and a['tradingClass'] == tradingClass and a['optionStrike'] == optionStrike and a['optionRight'] == optionRight:
                ex = 1
        if ex == 0: 
            #print('added', rid)   
            self.options.append({'expirationDate':expirationDate, 'tradingClass':tradingClass, 'optionStrike':optionStrike, 'optionRight':optionRight, 'contractDetails':contractDetails, 'id':rid})

    def setOptionTickData(self, rid, impliedVol, delta, optPrice,  gamma, vega, theta, undPrice):
        for d in self.getAllOptions():
            if d['id'] == rid:
                d["impliedVol"] = impliedVol
                d["delta"] = delta
                d["optPrice"] = optPrice
                d["gamma"] = gamma
                d["vega"] = vega
                d["theta"] = theta
                d["undPrice"] = undPrice
    
    def getAllOptions(self):
        return self.options
    
# class TicketOptionComputationGreeks:
#     def create(self, reqId, tickType, impliedVol, delta, optPrice, pvDividend, gamma, vega, theta, undPrice):
#         self.reqId = reqId
#         self.tickType = tickType
#         self.impliedVol = impliedVol
#         self.delta = delta
#         self.optPrice = optPrice
#         self.pvDividend = pvDividend
#         self.gamma = gamma
#         self.vega = vega
#         self.theta = theta
#         self.undPrice = undPrice


class FOPContracts:
    """
    class FOPContracts
    """
    @staticmethod
    def ES_FOP(lastDate, _strike, cp, tc):
        """ 
        ES_FOP::
        
            FOPContracts.ES_FOP()
            '''
            just call it this way !
            '''
            
        Args:
            none
        
        Returns:
            nothing
            
        This is a statement.

        .. warning::
        
           Never, ever, use this code!
        
        .. versionadded:: 0.0.1
        
        It's okay to use this code.
        
        .. note::

            Hallo Welt !
        
        .. graphviz::

           digraph {
              "From" -> "To";
           }
        
        .. graph:: foo

            "bar" -- "baz";
   
        """
        contract = Contract()
        contract.symbol = "ES"
        contract.secType = "FOP"
        contract.exchange = "GLOBEX"
        contract.currency = "USD"
        contract.lastTradeDateOrContractMonth = lastDate # "201712"
        contract.strike = _strike #2315
        contract.right = cp # "P"
        contract.multiplier = "50"
        contract.tradingClass = tc # "ES" ## ES, EW1, EW2, EW3, EW4
        return contract


    def getContractForConId(self, id):
        contract = Contract()
        contract.conId = id
        return contract

    @staticmethod
    def ES_FUT():
        contract = Contract()
        contract.symbol = "ES"
        contract.secType = "FUT"
        contract.exchange = "GLOBEX"
        contract.currency = "USD"
        contract.lastTradeDateOrContractMonth = "201803"
        #contract.strike = 2320
        #contract.right = "P"
        contract.multiplier = "50"
        #contract.tradingClass = "ES" ## ES, EW1, EW2, EW3, EW4
        return contract

    @staticmethod
    def ES_IDX():
        contract = Contract()
        contract.symbol = "ES"
        contract.secType = "IND"
        contract.exchange = "GLOBEX"
        contract.currency = "USD"
        return contract

    @staticmethod
    def VIX_IDX():
        contract = Contract()
        contract.symbol = "VIX"
        contract.secType = "IND"
        contract.exchange = "CBOE"
        contract.currency = "USD"
        return contract

    @staticmethod
    def OVX_IDX():
        contract = Contract()
        contract.symbol = "OVX"
        contract.secType = "IND"
        contract.exchange = "CBOE"
        contract.currency = "USD"
        return contract

    @staticmethod
    def DAX_IDX():
        contract = Contract()
        contract.symbol = "DAX"
        contract.secType = "IND"
        contract.exchange = "DTB"
        contract.currency = "EUR"
        return contract
    
    @staticmethod
    def GVZ_IDX():
        contract = Contract()
        contract.symbol = "GVZ"
        contract.secType = "IND"
        contract.exchange = "CBOE"
        contract.currency = "USD"
        return contract

    @staticmethod
    def JYVIX_IDX():
        contract = Contract()
        contract.symbol = "JYVIX"
        contract.secType = "IND"
        contract.exchange = "CBOE"
        contract.currency = "USD"
        return contract

    @staticmethod
    def EUVIX_IDX():
        contract = Contract()
        contract.symbol = "EUVIX"
        contract.secType = "IND"
        contract.exchange = "CBOE"
        contract.currency = "USD"
        return contract

    @staticmethod
    def V1X_IDX():
        contract = Contract()
        contract.symbol = "V1X"
        contract.secType = "IND"
        contract.exchange = "DTB"
        contract.currency = "EUR"
        return contract
    
    @staticmethod
    def TYVIX_IDX():
        contract = Contract()
        contract.symbol = "TYVIX"
        contract.secType = "IND"
        contract.exchange = "CBOE"
        contract.currency = "USD"
        return contract

    @staticmethod
    def VXSLV_IDX():
        contract = Contract()
        contract.symbol = "VXSLV"
        contract.secType = "IND"
        contract.exchange = "CBOE"
        contract.currency = "USD"
        return contract
    
    @staticmethod
    def BPVIX_IDX():
        contract = Contract()
        contract.symbol = "BPVIX"
        contract.secType = "IND"
        contract.exchange = "CBOE"
        contract.currency = "USD"
        return contract