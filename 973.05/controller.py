#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Controller Module

The Controller Module initiates Model, TWS and View and is the root node for all actions and data

.. inheritance-diagram:: controller
   :parts: 2
"""
from view import *
from tws import  *
from model import * 
from observer import *
import sqlite3
from database import IBDatabase

import quandl
 
class Controller(): 
    """Controller is the root class hosting the Controller functionality"""
    def __init__(self):
        """startup the Controller class and instantiate Model, View and TWS and connectToIB and return a Controller object"""  
        self.view = View(self)  ## forward Controller reference to view, create rootWidget
        self.tws = TWSWrapper(self, self.view.rootWindow)
        
        self.productCollection = OptionTrading_Product_Collection()  
        self.view.rootWindow.setProductCollection( self.productCollection ) ## teile dem Frame mit, wie es auf das Model zugreifen kann
        self.productCollection.setRootFrame(self.tws.rootWindow)  ## teile der PC mit, wie es auf das GUI gelangen kann

        
        self.userAction = UserAction(self)
        self.historicalData = HistoricalData(self)
        self.database = IBDatabase()
                
        self.reqHistData = []  ## unschäne Zwischenlösung !! REFACTOR STEP #030:  Refaktoriere dieses Array, was benötigt wird für HistoricalData
 
        print("loading COT data")
        self.quandData = QuandLoader(self)
        self.quandData.load_COT_data()
        print(self.quandData.get_cot("ES"))
 
#################################### initialization and setup routines ##################################### 
    def main_step_001_connectToIB(self):
        """being called from main.py: connects to TWS in an asynchronous way using Threads
        
        .. uml::  
   
           @startuml
              title main_step_001_connectToIB

                Main -> Controller: main_step_001_connectToIB
                Controller --> TWSWrapper: connect("127.0.0.1", 7497, 0)
                Controller --> TWSWrapper: run  (Thread(target = w.run))
                
           @enduml
        
        Die nächste Aktion erfolgt über "connectAck" im TWSWrapper, sobald die Verbindung hergestellt ist.
        """
        w = self.tws
        w.connect("127.0.0.1", 7497, 0)
        print("serverVersion:%s connectionTime:%s" % (w.serverVersion(), w.twsConnectionTime()))
        
        self.thread = Thread(target = w.run)
        self.thread.start()
        print('\n________________________________________________________________________\n')

    def main_step_002_runMainloop(self):    
        """being called from main.py since this is the last code line to be reached: starts the TKinter view and runs in a mainloop; """
        print("starting GUI")
        self.view.runMainloop()  ## run infinite mainloop
        print("====  >>> Mainloop stopped manually <<<")


################################ TWS callback routines for tws.py  #######################    
    def tws_step_003_connectAck(self):
        """being initiated from tws: get all managed account data from TWS
        
        .. uml::  
   
           @startuml
              title Product and Future Loading Procedure

                TWSWrapper --> Controller: tws_step_003_connectAck
                Controller --> Controller: populateProducts
                ProductCollection --> Product: requestFuturesForSingleProduct
                Product --> TWSWrapper: reqContractDetails
                
           @enduml
        
        Es werden die Produkte in der ProductCollection eingetragen und alle assozierten Futures zu jedem Produkt angefragt.
        Die Daten zu den Futures (conId etc.) kommen zurück über TWSWrapper -> contractDetails.
        
        """
        self.tws.reqManagedAccts() ### holen wir uns mal die Account-Namen, die mit diesem Login erreicht werden können
        print("controller - tws_step_003_connectAck - reqManagedAccounts")
        self.populateProducts()  ### jetzt laden wir die Produkte aus der Datenbank
        print("controller - tws_step_003_connectAck")
        #=======================================================================
        # self.view.rootWindow.setProductCollection( self.productCollection ) ## teile dem Frame mit, wie es auf das Model zugreifen kann
        # self.productCollection.setRootFrame(self.tws.rootWindow)  ## teile der PC mit, wie es auf das GUI gelangen kann
        #=======================================================================

    def populateProducts(self): 
        """populateProducts
        
        .. uml::  
   
           @startuml
              title Product and Future Loading Procedure

                Controller --> Controller: populateProducts
                ProductCollection --> Product: requestFuturesForSingleProduct
                Product --> TWSWrapper: reqContractDetails (asynchron)
                
           @enduml
        
        Es werden die Produkte in der ProductCollection eingetragen und alle assoziierten Futures zu jedem Produkt angefragt.
        Die Daten zu den Futures (conId etc.) kommen zurück über TWSWrapper -> contractDetails.        
        
        """   
        db = IBDatabase()
        for row in db.DBcursor.execute('SELECT ID_PRODUCT, PRODUCT_SYMBOL, PRODUCT_SECTYPE, PRODUCT_EXCHANGE, PRODUCT_CURRENCY, PRODUCT_MULTIPLIER FROM PRODUCTS ORDER BY ID_PRODUCT'):  ##WHERE PRODUCT_SYMBOL = "ES" 
            print(row[0], " : ", row)
            self.productCollection.addProduct( OptionTrading_Product(self.productCollection, row[0], row[1], row[2], row[3], row[4], row[5], self.quandData.get_cot(row[1]) ))
        db.closeDatabase()
        #db.DBconnection.commit()
            
        self.productCollection.requestFuturesForAllProducts(self.tws)  ### <<< hier ist der Einstieg für das Laden der Futures zu den Produkten; es werden noch keine Optionsketten geholt ! >>>
        ## ACHTUNG: die futures kommen asynchron zurück; sobald ein contractDetailsEnd zu einem produkt kommt, wird "fetchOptionChains" im OptionTrading_Product ausgelöst, was reqSecDefOptParams nach sich zieht
        
        
    def managedAccounts(self, accountsList: str):
        print("Account list: ", accountsList)
        account = accountsList.split(",")[0]
        self.tws.accountUpdateService = TWSAccountUpdateService(self, account)
        self.tws.accountUpdateService.accountValueObserver.register(self)
        self.tws.accountUpdateService.portfolioObserver.register(self)
        self.tws.reqAccountUpdates(True, account)
     
    def accountValueChanged(self, accountName: str):
        """accountValueChanged
        
        .. uml::  
   
           @startuml
              title hole Daten der Vola-Indizes und aktualisiere das Chart

                Controller -> Controller: trigger_volaChartData
                Controller --> Controller: requestHistoricalData
                Controller --> TWSWrapper: reqHistoricalData
                                
           @enduml
        
        """
        self.tws.reqAccountUpdates(True, accountName)  
        print("controller.py - reqAccountUpdates = True")
        self.trigger_volaChartData() 
        
    def portfolioChanged(self, accountPositions):
        print("controlly.py - portfolioChanged: ", accountPositions)
        
    def securityDefinitionOptionParameterEnd(self, reqId: int):
        self.productCollection.notifyAllOptionChainsAreDeliveredForReqId(reqId, self.tws) 


################################ TWS request routines for tws.py  #######################   

    def trigger_volaChartData(self):   
        db = IBDatabase() 
        for row in db.DBcursor.execute('SELECT PRODUCT_SYMBOL, VOLA_SYMBOL, VOLA_SECTYPE, VOLA_EXCHANGE, VOLA_CURRENCY, ID_VOLA FROM PRODUCTS CROSS JOIN VOLA_INDEX ON PRODUCTS.ID_PRODUCT = VOLA_INDEX.PRODUCT_ID  '):
            print(row[0], " : ", row)
            p = self.productCollection.getProductBySymbol(row[0])
            contract = Contract()
            contract.symbol = row[1]
            contract.secType = row[2]
            contract.exchange = row[3]
            contract.currency = row[4]
            v = OptionTrading_VolatilityProduct(contract)
            p.setVolatilityProduct(v)
            #self.getVolaChartData(p.getVolatilityProduct())
            vp = p.getVolatilityProduct()
            #self.productCollection.addProduct( OptionTrading_Product(self.productCollection, row[1], row[2], row[3], row[4]))
            if row[5] != None:
                 #self.reqHistData.append({'key' : row[5], 'volaProd' : vp})
                 self.historicalData.appendVolaObserver(row[5],  vp)
                 print(str(row[5]), " requested: reqHistoricalData ", datetime.datetime.now())
                 self.tws.reqHistoricalData(row[5], vp.getContractDetails(), datetime.datetime.today().strftime("%Y%m%d %H:%M:%S"), "12 M", "1 day", "TRADES", 0, 1,  False, [])

        db.closeDatabase()
        
    #===========================================================================
    # def getVolaChartData(self, volaProd):    ### ES/VIX, CL/OVX, EUR/EUVIX, GC/GVZ, ZB/TYVIX
    #      print("controller.py - getVolaChartData: ", volaProd, " ", volaProd.getContractDetails().symbol)
    #      theKey = None   
    #      if volaProd.getContractDetails().symbol == "VIX":
    #          theKey = 9900
    #      if volaProd.getContractDetails().symbol == "OVX":
    #          theKey = 9901 
    #      if volaProd.getContractDetails().symbol == "EUVIX":
    #          theKey = 9902 
    #      if volaProd.getContractDetails().symbol == "GVZ":
    #          theKey = 9903 
    #      if volaProd.getContractDetails().symbol == "TYVIX":
    #          theKey = 9904 
    #      if volaProd.getContractDetails().symbol == "VXSLV":
    #          theKey = 9905 
    #      if volaProd.getContractDetails().symbol == "BPVIX":
    #          theKey = 9906 
    #      if volaProd.getContractDetails().symbol == "VXXLE":
    #          theKey = 9907 
    #                                     
    #      if theKey != None:
    #          self.reqHistData.append({'key' : theKey, 'volaProd' : volaProd})
    #          print(str(theKey), " requested: reqHistoricalData ", datetime.datetime.now())
    #          self.tws.reqHistoricalData(theKey, volaProd.getContractDetails(), datetime.datetime.today().strftime("%Y%m%d %H:%M:%S"), "12 M", "1 day", "TRADES", 0, 1,  False, [])
    #  #        self.TWSWrapper.reqHistoricalData(9900, volaProd.getContractDetails(), datetime.datetime.today().strftime("%Y%m%d %H:%M:%S"), "2 W", "30 mins", "TRADES", 1, 1,  False, [])
    #===========================================================================

 

class HistoricalData(Observable):
    """
    is being initiated by Controller class at creation time of controller 
    """
    def __init__(self, aController: Controller):
        self.controller = aController
        self.volaObserver = []
        self.futureObserver = []
        
    def historicalData(self, reqId:int, bar: BarData):
        for a in self.volaObserver:
            if int(a['key']) == reqId:
                a['volaProd'].addBar(bar)
                print("volaProd Bar: ",reqId, bar.date, bar.close)
        #=======================================================================
        # for a in self.controller.reqHistData:
        #     if int(a['key']) == reqId:
        #         a['volaProd'].addBar(bar)
        #         print("reqHistData Bar: ",reqId, bar.date, bar.close)
        #=======================================================================
        for a in self.futureObserver:
            if int(a['key']) == reqId:
                a['future'].addBar(bar)
                print("futureObserver Bar: ",reqId, bar.date, bar.close)
     
    def historicalDataEnd(self, reqId:int, start:str, end:str):
        """remove reqId from list self.reqHistData"""
        for a in self.volaObserver:
            if int(a['key']) == reqId:
                a['volaProd'].historicalDataEnd()
                self.volaObserver = [d for d in self.volaObserver if d['key'] != reqId]
        #=======================================================================
        # for a in self.controller.reqHistData:
        #     if int(a['key']) == reqId:
        #         a['volaProd'].historicalDataEnd()
        #         self.controller.reqHistData = [d for d in self.controller.reqHistData if d['key'] != reqId]
        #         print(">>>>>>>>>>>> self.controller.reqHistData", reqId, start, end)
        #======================================================================= 
        for a in self.futureObserver:
            if int(a['key']) == reqId:
                a['future'].historicalDataEnd()
                self.futureObserver = [d for d in self.futureObserver if d['key'] != reqId]
        print("controller.py - HistoricalData ended: ", reqId, datetime.datetime.now())   

    def historicalDataUpdate(self, reqId:int, bar: BarData):
        print("HistoricalDataUpdate. ", reqId, " Date:", bar.date, "Open:", bar.open,
            "High:", bar.high, "Low:", bar.low, "Close:", bar.close, "Volume:", bar.volume,
            "Count:", bar.barCount, "WAP:", bar.average)
 
    def appendVolaObserver(self, key, vp:OptionTrading_VolatilityProduct):
        self.volaObserver.append({'key' : key, 'volaProd' : vp})
        
    def appendFutureObserver(self, key, f:OptionTrading_Future):
        self.futureObserver.append({'key' : key, 'future' : f})



################################ UserAction routines for view.py  #######################            
class UserAction:
    def __init__(self, aController: Controller):
        self.controller = aController
    
    def trigger_volaChartData(self):
        """request historical Data"""
        self.controller.trigger_volaChartData()

    def triggerTWStoFetchFutureHistoricalDataAndOptionChainGreeks(self, comboboxValue, aStockChart):
        self.controller.productCollection.triggerTWStoFetchFutureHistoricalDataAndOptionChainGreeks(comboboxValue, self.controller, aStockChart)
        

class QuandLoader:
    def __init__(self, aController):
        self.controller = aController
        self.cot = []
    
    def load_COT_data(self):
        db = IBDatabase()
        for row in db.DBcursor.execute('SELECT ID_PRODUCT, PRODUCT_SYMBOL, PRODUCT_QUANDL_COT FROM PRODUCTS ORDER BY ID_PRODUCT'):  ##WHERE PRODUCT_SYMBOL = "ES" 
            self.cot.append({'id':row[0], 'key':row[1], 'quandl':row[2], 'cot':None, 'date':None, 'long':None, 'short':None})
        db.closeDatabase()
        
        quandl.ApiConfig.api_key = "F2jMgDVZZputbTm6E1Uo" ### meine ID !!!
        for a in self.cot:
            a['cot'] = quandl.get(a['quandl'], returns="numpy")
            
        for a in self.cot:
            a['date'] = [el[0] for el in a['cot']]
            a['long'] = [el[2] for el in a['cot']]
            a['short'] = [el[3] for el in a['cot']]
            
        self.pickleDump()

    def get_cot(self, aKey):
        for a in self.cot:
            if aKey == a['key']:
                return a
            
    def pickleDump(self):
        pickle.dump(self.cot, open("quandloader.data","wb"))
        #with open("quandloader.data", 'r', errors='replace') as f:
        #    print(f.readlines())
        
    def pickleLoad(self):
        self.cot = pickle.load(open("quandloader.data","rb"))
        #with open("quandloader.data", 'r', errors='replace') as f:
        #    print(f.readlines())

    #===========================================================================
    # def update_COT_data(self):        
    #     db = IBDatabase()
    #     execString = "INSERT INTO CONTRACT_DETAILS_FUTURES (FUTURE_PRODUCT_ID, FUTURE_CONID, FUTURE_SYMBOL, FUTURE_SECTYPE, FUTURE_LASTTRADEORCONTRACTMONTH, FUTURE_MULTIPLIER, FUTURE_EXCHANGE, FUTURE_CURRENCY) VALUES (" + str(reqId) + ", " + str(contractDetails.summary.conId) + " , '" + contractDetails.summary.symbol + "', '" +  contractDetails.summary.secType + "', '" + contractDetails.summary.lastTradeDateOrContractMonth + "', '" + contractDetails.summary.multiplier + "', '" + contractDetails.summary.exchange + "', '" + contractDetails.summary.currency+ "')" 
    #     try:
    #         db.DBcursor.execute(execString)
    #         db.DBconnection.commit()
    #     except sqlite3.Error as e:
    #         if str(e) != "UNIQUE constraint failed: CONTRACT_DETAILS_FUTURES.FUTURE_CONID":   ## this error simply means that the Future is already in our database
    #             print("error: ", e)
    #     db.closeDatabase()
    #===========================================================================
