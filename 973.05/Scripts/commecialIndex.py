
study("COT Index by BW", shorttitle="COT Commercial Index", precision=1)

longStudy = input(title="Long Study", defval=156)
shortStudy=input(title="Short Study", defval=26)

qticker =
     syminfo.root == "ZC" ? "C"  :
     syminfo.root == "AU" ? "AD"  :
     syminfo.root == "ZW" ? "W"  :
     syminfo.root == "ZS" ? "S"  :
     syminfo.root == "ZL" ? "BO" :
     syminfo.root == "ZM" ? "SM" :
     syminfo.root == "HG" ? "HG" :
     syminfo.root == "GC" ? "GC" :
     syminfo.root == "SI" ? "SI" :
     syminfo.root == "CC" ? "CC" :
     syminfo.root == "KC" ? "KC" :
     syminfo.root == "CT" ? "CT" :
     syminfo.root == "OJ" ? "OJ" :
     syminfo.root == "SB" ? "SB" :
     syminfo.root == "CL" ? "CL" :
     syminfo.root == "HO" ? "HO" :
     syminfo.root == "NG" ? "NG" :
     syminfo.root == "ZN" ? "TY" :
     syminfo.root == "ZB" ? "US" :
     syminfo.root == "B6" ? "BP" :
     syminfo.root == "ES" ? "SPC" :
     syminfo.root == "E6" ? "EC" :
     syminfo.root == "JPY" ? "JY" :
     syminfo.root == "PR" ? "CD" :
     syminfo.root == "J23" ? "HO" :
     syminfo.root == "RB" ? "RB" :
     syminfo.root == "LE" ? "LC" :
     syminfo.root == "HE" ? "LH" :
     syminfo.root == "LB" ? "LB" :
     syminfo.root == "WN" ? "SF" :
     syminfo.root == "MG" ? "AD" :
     syminfo.root == "VIX" ? "VX" :
     ""

force_length = input(104, title="Weeks (0 = automatic)")

length =
     force_length != 0 ? force_length :
     syminfo.root == "CC" ? 13 :
     syminfo.root == "KC" ? 13 :
     syminfo.root == "CT" ? 13 :
     syminfo.root == "OJ" ? 13 :
     syminfo.root == "SB" ? 13 :
     syminfo.root == "CL" ? 13 :
     syminfo.root == "HO" ? 13 :
     syminfo.root == "NG" ? 13 :
     syminfo.root == "ZN" ? 13 :
     syminfo.root == "E6" ? 13 :
     syminfo.root == "AD" ? 13 :
     26

GetDailyAdjustment(weeks) =>
    weekCount = weeks
    daily_adjust = 1
    tmp = for i = 0 to (length * 5)
        weekCount := weekCount - iff(dayofweek[i] < dayofweek[i+1], 1, 0)
        if weekCount <= 0
            break
        daily_adjust := daily_adjust + 1
    daily_adjust

Highest(x, y) =>
    ret = x
    for i = 1 to y-1
        ret := max(ret, x[i])

Lowest(x, y) =>
    ret = x
    for i = 1 to y-1
        ret := min(ret, x[i])


legacy_cot = "QUANDL:CFTC/" + qticker + "_FO_L_ALL|"
cot = "QUANDL:CFTC/" + qticker + "_FO_ALL|"

oi = security(legacy_cot + "0", "W", close)
 
no_cot_adjst = oi == oi[1] ? 1 : 0
length_adjst = isdaily ? GetDailyAdjustment(length + no_cot_adjst) : length + no_cot_adjst

comm_lg = security(legacy_cot + "4", "W", close)
comm_sh = security(legacy_cot + "5", "W", close)
comm_net = comm_lg - comm_sh

comm_max = Highest(comm_net, longStudy)
comm_min = Lowest(comm_net, longStudy)
comm_idx = if (dayofweek > nz(dayofweek[1]))
    comm_idx[1]
else
    100 * (comm_net - comm_min) / (comm_max - comm_min)
    
comm_max05 = Highest(comm_net, shortStudy)
comm_min05 = Lowest(comm_net, shortStudy)
comm_idx05 = if (dayofweek > nz(dayofweek[1]))
    comm_idx05[1]
else
    100 * (comm_net - comm_min05) / (comm_max05 - comm_min05)


p1 = plot(comm_idx, color=blue, title="Index", style=line, linewidth=3)
p2 = plot(comm_idx05, color=purple, title="Index", style=line, linewidth=2)
h1 = plot(80, color = green, title="Buy", style=line, linewidth=2)
h2 = plot(20, color = red, title="Sell", style=line, linewidth=2)


 